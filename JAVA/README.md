# IssueTracker

REST endpoint documentation as of 09/30/2019.

Virtual root: `/IWebProg3/`

Endpoints:

* Authenticate user credentials: `POST /Authenticate/v1`
* Fetch the newest user complaints: `GET /GetComplaints/v1`

## `POST /Authenticate/v1`
Authenticate user credentials.

Parameters:

* `username`: username
* `password`: password

Result layout:

An **AuthenticationResult** object containing:

* **Status**, a boolean value indicating whether the authentication succeeded
* **Message**, a human-readable error message describing the problem (present only when **Status** is false)
* **ErrorCode**, an error code indicating what went wrong (present only when **Status** is false)
* **UserID**, the identifier of the user (present only when **Status** is true)

Result examples:
On success when the request wasn't malformed and the credentials were correct:
```
<AuthenticationResult>
	<Status>true</Status>
	<UserID>1</UserID>
</AuthenticationResult>
```

If the supplied credentials are invalid, then the following response will be sent:
```
<AuthenticationResult>
	<Status>false</Status>
	<Message>Bad credentials</Message>
	<ErrorCode>EACCES</ErrorCode>
	<UserID>-1</UserID>
</AuthenticationResult>
```

If one or more required parameters are missing, then the following response
will be sent:
```
<AuthenticationResult>
	<Status>false</Status>
	<Message>Bad arguments</Message>
	<ErrorCode>EINVAL</ErrorCode>
	<UserID>-1</UserID>
</AuthenticationResult>
```

## `GET /GetComplaints/v1`
Return the list of the newest customer complaints.

Parameters:

None.

Result layout:

A **ComplaintsResult** root element, an array of complaints that contain:

* **Author**: author of the complaint
* **Contents**: the contents of the complaint

Result examples:

```
<ComplaintsResult>
	<Complaint>
		<Author>Example Author</Author>
		<Contents>
			Example contents.
		</Contents>
	</Complaint>
</ComplaintsResult>
```

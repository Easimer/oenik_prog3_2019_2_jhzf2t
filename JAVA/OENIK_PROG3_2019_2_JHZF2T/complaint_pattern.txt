﻿stmtFeature = program | hozzaadas | torles | kereses | lekerdezes
stmtEntitast = alkalmazottat | osztalyt | munkaelozmenyt | allast | helyszint | orszagot | regiot

stmtUserTevekenyseg = hozzaadni egy uj {stmtEntitast} | torolni egy {stmtEntitast} | megkeresni egy {stmtEntitast} | lekerdezni tobb {stmtEntitast}

stmtUserProblema = hibat ir ki | nem csinal semmit | osszeomlik a program

stmtSwearing = (hogy a fenébe nem tudjak rendesen megcsinálni | mi a faszért nem működik normálisan | basszák meg mind | kurva isten faszát már | a rák egye ki ezt a szart) | [{stmtSwearing}]

stmtNemMukodik = Nem mukodik a {stmtFeature}!
stmtProbalkozas = Probaltam {stmtUserTevekenyseg}, de {stmtUserProblema}.
stmtMastCsinal = Ha megpróbálok {stmtUserTevekenyseg} akkor helyette {stmtUserProblema}.

stmtPanasz = [{stmtNemMukodik}] {stmtProbalkozas | stmtMastCsinal} [{stmtSwearing}]
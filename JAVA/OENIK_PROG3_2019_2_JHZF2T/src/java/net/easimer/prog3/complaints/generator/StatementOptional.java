package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementOptional implements BaseStatement {
    private BaseStatement m_stmt;
    
    public StatementOptional(BaseStatement stmt) {
        m_stmt = stmt;
    }
    
    @Override
    public String getStatementText() {
        String ret = "";
        
        if(m_stmt != null) {
            ret = m_stmt.getStatementText();
        }
        
        return ret;
    }
    
}

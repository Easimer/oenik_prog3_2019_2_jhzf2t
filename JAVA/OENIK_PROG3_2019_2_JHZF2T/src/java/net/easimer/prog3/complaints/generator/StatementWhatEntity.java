package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementWhatEntity implements BaseStatement {
    private String m_entity;
    
    private static String[] entities = new String[] {
        "alkalmazottat", "osztályt", "munkaelőzményt", "állást", "helyszínt",
        "országot", "régiót"
    };
    
    public StatementWhatEntity(Random rnd) {
        m_entity = entities[rnd.nextInt(entities.length)];
    }
    
    @Override
    public String getStatementText() {
        return m_entity;
    }
}

package net.easimer.prog3.complaints.generator;

import java.util.Random;

/**
 *
 * @author danielm
 */
public class StatementSwearing implements BaseStatement {
    private static Boolean disabled = false;
    private static double chanceNext = 0.60;
    private String m_text;
    private BaseStatement m_next;
    private static String[] texts = new String[] {
        "hogy a fenébe nem tudják rendesen megcsinálni",
        "mi a fenéért nem működik normálisan",
    };
    
    public StatementSwearing(Random rnd) {
        m_text = texts[rnd.nextInt(texts.length)];
        StatementSwearing next = null;
        
        if(rnd.nextDouble() < chanceNext) {
            next = new StatementSwearing(rnd);
        }
        m_next = new StatementOptional(next);
    }

    @Override
    public String getStatementText() {
        String ret = "";
        
        if(!disabled) {            
            ret = String.format("%s %s", m_text, m_next.getStatementText());
        }
        
        return ret;
    }
}

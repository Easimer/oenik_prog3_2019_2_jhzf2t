package net.easimer.prog3.authentication;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author danielm
 */
@WebServlet(name = "Authentication", urlPatterns = {"/Authenticate/v1"})
public class Authentication extends HttpServlet {
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String username, password;
            AuthenticationResult res;
            JAXBContext xctx;
            Marshaller marshaller;
            
            username = request.getParameter("username");
            password = request.getParameter("password");
            
            if(username != null && password != null) {
                // NOTE(danielm): durga
                if(password.length() >= 5 && password.charAt(1) == 'u' &&
                        password.charAt(4) == 'a') {
                    res = new AuthenticationResult(1);
                } else {
                    res = new AuthenticationResult("Bad credentials", "EACCES");
                }
            } else {
                res = new AuthenticationResult("Bad arguments", "EINVAL");
            }
            
            try {
                xctx = JAXBContext.newInstance(AuthenticationResult.class);
                marshaller = xctx.createMarshaller();
                marshaller.marshal(res, out);
            } catch(JAXBException ex) {
                // TODO(danielm): uh oh
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Authentication endpoint";
    }// </editor-fold>

}

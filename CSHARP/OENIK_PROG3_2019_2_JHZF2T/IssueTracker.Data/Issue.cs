﻿// <copyright file="Issue.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Data
{
    using System;

    /// <summary>
    /// Issue entity.
    /// </summary>
    public class Issue : IEntityWithID
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Issue"/> class.
        /// </summary>
        public Issue()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Issue"/> class.
        /// </summary>
        /// <param name="other">Other instance.</param>
        public Issue(Issue other)
        {
            this.ID = other.ID;
            this.Project = other.Project;
            this.Title = other.Title;
            this.AssignedTo = other.AssignedTo;
            this.Solved = other.Solved;
            this.CreationDate = other.CreationDate;
            this.Tag = other.Tag;
            this.Prio = other.Prio;
            this.ProjectRef = other.ProjectRef;
            this.AssignedToRef = other.AssignedToRef;
        }

        /// <summary>
        /// Gets or sets the issue identifier.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): IDs are automatically generated.
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the project this issue is related to.
        /// </summary>
        [IgnoredByEditor]
        public int Project { get; set; }

        /// <summary>
        /// Gets or sets the issue title.
        /// </summary>
        [EditorRequired]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the user this issue was assigned to.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): hidden because IssueInfo will hide this prop
        public int? AssignedTo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the issue was solved.
        /// </summary>
        [EditorDescription("Mark as solved")]
        public bool Solved { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the date when the issue was opened.
        /// </summary>
        [EditorRequired]
        [EditorDescription("Created on")]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the tag of this issue.
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the priority of this issue.
        /// </summary>
        [EditorDescription("Priority")]
        public int Prio { get; set; }

        /// <summary>
        /// Gets or sets the project this issue belongs to.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): hidden because IssueInfo will hide this prop
        [EditorRequired]
        public virtual Project ProjectRef { get; set; }

        /// <summary>
        /// Gets or sets the user this issue is assigned to.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): hidden because IssueInfo will hide this prop
        public virtual User AssignedToRef { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is Issue other)
            {
                ret = this.ID == other.ID;
                ret &= this.Project == other.Project;
                ret &= this.Title == other.Title;
                ret &= this.AssignedTo.Equals(other.AssignedTo);
                ret &= this.Solved == other.Solved;
            }

            return ret;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int ret = 13;

            ret = (ret * 7) + this.ID.GetHashCode();
            ret = (ret * 7) + this.Project.GetHashCode();
            ret = (ret * 7) + this.Title.GetHashCode();
            ret = (ret * 7) + this.AssignedTo.GetHashCode();
            ret = (ret * 7) + this.Solved.GetHashCode();

            return ret;
        }
    }
}

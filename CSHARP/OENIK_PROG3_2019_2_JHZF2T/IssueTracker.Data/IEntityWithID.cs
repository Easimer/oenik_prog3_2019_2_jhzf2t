﻿// <copyright file="IEntityWithID.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Data
{
    /// <summary>
    /// Entity with an ID field.
    /// </summary>
    public interface IEntityWithID
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        int ID { get; set; }
    }
}

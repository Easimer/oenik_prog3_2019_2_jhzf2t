﻿// <copyright file="User.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Data
{
    using System;

    /// <summary>
    /// User entity.
    /// </summary>
    public class User : IEntityWithID
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="ent">User entity.</param>
        public User(User ent)
        {
            this.ID = ent.ID;
            this.Username = ent.Username;
            this.FirstName = ent.FirstName;
            this.LastName = ent.LastName;
            this.EmailAddr = ent.EmailAddr;
            this.Telephone = ent.Telephone;
        }

        /// <summary>
        /// Gets or sets the user's ID number.
        /// </summary>
        [IgnoredByEditor] // NOTE(danielm): IDs are automatically generated.
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the user's username.
        /// </summary>
        [EditorRequired]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the user's first name.
        /// </summary>
        [EditorRequired]
        [EditorDescription("First name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the user's last name.
        /// </summary>
        [EditorRequired]
        [EditorDescription("Last name")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the user's e-mail address.
        /// </summary>
        [EditorDescription("Email address")]
        public string EmailAddr { get; set; }

        /// <summary>
        /// Gets or sets the user's telephone number.
        /// </summary>
        public string Telephone { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            bool ret = false;
            if (obj is User)
            {
                var other = obj as User;
                ret = this.ID == other.ID &&
                    this.FirstName == other.FirstName &&
                    this.LastName == other.LastName &&
                    this.Username == other.Username &&
                    this.EmailAddr == other.EmailAddr &&
                    this.Telephone == other.Telephone;
            }

            return ret;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            int ret = 13;
            ret = (ret * 7) + this.ID.GetHashCode();
            ret = (ret * 7) + this.Username.GetHashCode();
            ret = (ret * 7) + this.FirstName.GetHashCode();
            ret = (ret * 7) + this.LastName.GetHashCode();
            ret = (ret * 7) + this.EmailAddr.GetHashCode();
            ret = (ret * 7) + this.Telephone.GetHashCode();
            return ret;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format(
                "#{0} '{1}' '{2} {3}' {4} {5}",
                this.ID,
                this.Username,
                this.FirstName,
                this.LastName,
                this.EmailAddr,
                this.Telephone);
        }
    }
}

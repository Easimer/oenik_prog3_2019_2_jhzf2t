﻿// <copyright file="EditorDescriptionAttribute.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database
{
    using System;

    /// <summary>
    /// Enables an entity field to have a human-readable name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true)]
    public class EditorDescriptionAttribute : Attribute
    {
        private string description;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorDescriptionAttribute"/> class.
        /// </summary>
        /// <param name="description">This field's description.</param>
        public EditorDescriptionAttribute(string description)
        {
            this.description = description;
        }

        /// <summary>
        /// Gets the field's description.
        /// </summary>
        public string Description => this.description;
    }
}

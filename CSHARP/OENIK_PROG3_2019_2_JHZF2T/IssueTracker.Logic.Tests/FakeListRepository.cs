﻿// <copyright file="FakeListRepository.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// A fake, list-based repository implementation.
    /// </summary>
    /// <typeparam name="T">Type of the entity contained in this repository.</typeparam>
    internal class FakeListRepository<T> : IRepository<T>
        where T : class, IEntityWithID
    {
        private LinkedList<T> entities;
        private FaultyModeFlags faultyModeFlags;

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeListRepository{T}"/> class.
        ///
        /// At construction the repository is empty and none of the operations
        /// will be in faulty mode.
        /// </summary>
        public FakeListRepository()
        {
            this.entities = new LinkedList<T>();
            this.faultyModeFlags = FaultyModeFlags.None;
        }

        /// <summary>
        /// Flags specifying what operations should be faulty.
        /// </summary>
        [Flags]
        public enum FaultyModeFlags
        {
            /// <summary>
            /// No operations will fail.
            /// </summary>
            None = 0,

            /// <summary>
            /// Create() will fail.
            /// </summary>
            Create = 1 << 0,

            /// <summary>
            /// Delete() will fail.
            /// </summary>
            Delete = 1 << 1,

            /// <summary>
            /// GetById() will fail.
            /// </summary>
            GetById = 1 << 2,

            /// <summary>
            /// ReadAll() will fail.
            /// </summary>
            ReadAll = 1 << 3,

            /// <summary>
            /// All operations will fail.
            /// </summary>
            All = -1,
        }

        /// <summary>
        /// Gets the list of entities.
        /// </summary>
        public LinkedList<T> Entities => this.entities;

        /// <summary>
        /// Gets or sets the faulty mode configuration of this repo.
        ///
        /// Depending on the flags (see <see cref="FaultyModeFlags"/>)
        /// operations can be configured to reliably fail: in faulty mode, Create() and
        /// Delete() returns zero, GetById returns null and ReadAll() returns
        /// an empty set.
        /// </summary>
        public FaultyModeFlags FaultyMode { get => this.faultyModeFlags; set => this.faultyModeFlags = value; }

        /// <inheritdoc/>
        public int Create(params T[] instance)
        {
            int ret = 0;
            if (!this.faultyModeFlags.HasFlag(FaultyModeFlags.Create))
            {
                foreach (var inst in instance)
                {
                    if (this.CreateCheck(inst))
                    {
                        this.entities.AddLast(inst);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }

                ret = instance.Length;
            }

            return ret;
        }

        /// <inheritdoc/>
        public int Delete(params T[] instance)
        {
            int ret = 0;

            if (!this.faultyModeFlags.HasFlag(FaultyModeFlags.Delete))
            {
                foreach (var inst in instance)
                {
                    this.entities.Remove(inst);
                }

                ret = instance.Length;
            }

            return ret;
        }

        /// <inheritdoc/>
        public T GetById(int id)
        {
            if (!this.faultyModeFlags.HasFlag(FaultyModeFlags.GetById))
            {
                foreach (var ent in this.entities)
                {
                    if (ent.ID == id)
                    {
                        return ent;
                    }
                }
            }

            return null;
        }

        /// <inheritdoc/>
        public IQueryable<T> ReadAll()
        {
            IQueryable<T> ret;

            if (!this.faultyModeFlags.HasFlag(FaultyModeFlags.ReadAll))
            {
                ret = this.entities.AsQueryable();
            }
            else
            {
                ret = new List<T>().AsQueryable();
            }

            return ret;
        }

        /// <summary>
        /// Called back to when Create(T) is called to check whether this entity
        /// violates the table constraints or not.
        /// </summary>
        /// <param name="value">Entity value.</param>
        /// <returns>Whether the entity respects all the constraints.</returns>
        protected virtual bool CreateCheck(T value)
        {
            return true;
        }
    }
}

﻿// <copyright file="UserManagementTests.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using System.Collections.Generic;
    using System.Linq;
    using Net.Easimer.Prog3.Database.Data;
    using Net.Easimer.Prog3.IssueTracker;
    using NUnit.Framework;

    /// <summary>
    /// User manager tests.
    /// </summary>
    [TestFixture]
    internal class UserManagementTests : LogicTestEnvironment
    {
        /// <summary>
        /// Test user creation.
        /// </summary>
        [Test]
        public void UserCreate()
        {
            List<User> testUsers = this.GenerateTestUsers();
            foreach (var testUser in testUsers)
            {
                this.ClearRepositories();
                Assert.That(this.RepoUser.Entities.Count, Is.EqualTo(0));
                this.UMan.RegisterNewUser(testUser.ID, testUser.Username, testUser.FirstName, testUser.LastName, testUser.EmailAddr, testUser.Telephone);
                Assert.That(this.RepoUser.Entities.Count, Is.EqualTo(1));
                Assert.That(this.RepoUser.Entities.First.Value, Is.EqualTo(testUser));
            }
        }

        /// <summary>
        /// Test user deletion.
        /// </summary>
        [Test]
        public void UserDelete()
        {
            List<User> testUsers = this.GenerateTestUsers();
            this.ClearRepositories();
            foreach (var testUser in testUsers)
            {
                this.RepoUser.Entities.AddLast(testUser);
            }

            for (int i = 0; i < testUsers.Count; i++)
            {
                Assert.That(this.RepoUser.Entities.Count, Is.EqualTo(testUsers.Count - i));
                this.UMan.DeleteUser(testUsers[i].ID);
                Assert.That(this.RepoUser.Entities.Count, Is.EqualTo(testUsers.Count - i - 1));
            }
        }

        /// <summary>
        /// Test user fetching.
        /// </summary>
        [Test]
        public void UserFetch()
        {
            List<User> testUsers = this.GenerateTestUsers();
            this.ClearRepositories();
            foreach (var testUser in testUsers)
            {
                this.RepoUser.Entities.AddLast(testUser);
            }

            var result = this.UMan.GetAllUsers().ToList();
            Assert.That(result.Count, Is.EqualTo(testUsers.Count));
            testUsers.ForEach(x => Assert.That(result.Any(y => y.Equals(new UserInfo(x)))));
        }

        /// <summary>
        /// Test user renaming.
        /// </summary>
        /// <param name="firstName">First name.</param>
        /// <param name="lastName">Last name.</param>
        [TestCase("Elek", "Teszt")]
        [TestCase("Elek", "Gipsz")]
        [TestCase("Erno", "Teszt")]
        [TestCase(null, "Gipsz")]
        [TestCase("Erno", null)]
        [TestCase(null, null)]
        public void UserRenaming(string firstName, string lastName)
        {
            this.ClearRepositories();
            this.UMan.RegisterNewUser(1, "test", "Elek", "Teszt");

            var resUser = this.UMan.RenameUser(1, firstName, lastName);

            if (firstName != null)
            {
                Assert.AreEqual(firstName, resUser.FirstName);
            }
            else
            {
                Assert.AreEqual("Elek", resUser.FirstName);
            }

            if (lastName != null)
            {
                Assert.AreEqual(lastName, resUser.LastName);
            }
            else
            {
                Assert.AreEqual("Teszt", resUser.LastName);
            }
        }

        /// <summary>
        /// Test user renaming by handle.
        /// </summary>
        /// <param name="firstName">First name.</param>
        /// <param name="lastName">Last name.</param>
        [TestCase("Elek", "Teszt")]
        [TestCase("Elek", "Gipsz")]
        [TestCase("Erno", "Teszt")]
        [TestCase(null, "Gipsz")]
        [TestCase("Erno", null)]
        [TestCase(null, null)]
        public void UserRenamingByHandle(string firstName, string lastName)
        {
            this.ClearRepositories();

            var user = new User();
            user.ID = 1;
            user.Username = "test";
            user.FirstName = "Elek";
            user.LastName = "Teszt";
            this.RepoUser.Entities.AddLast(user);
            var userInfo = new UserInfo(user);

            var resUser = this.UMan.RenameUser(userInfo, firstName, lastName);

            if (firstName != null)
            {
                Assert.AreEqual(firstName, resUser.FirstName);
            }
            else
            {
                Assert.AreEqual("Elek", resUser.FirstName);
            }

            if (lastName != null)
            {
                Assert.AreEqual(lastName, resUser.LastName);
            }
            else
            {
                Assert.AreEqual("Teszt", resUser.LastName);
            }
        }

        /// <summary>
        /// Test finding a user by ID.
        /// </summary>
        [Test]
        public void UserFindById()
        {
            List<User> testUsers = this.GenerateTestUsers();
            this.ClearRepositories();
            foreach (var testUser in testUsers)
            {
                this.RepoUser.Entities.AddLast(testUser);
            }

            for (int i = 0; i < testUsers.Count; i++)
            {
                var res = this.UMan.GetUserByID(testUsers[i].ID);
                Assert.That(res, Is.Not.EqualTo(null));
                Assert.That(res.ID, Is.EqualTo(testUsers[i].ID));
                Assert.That(res, Is.EqualTo(new UserInfo(testUsers[i])));
            }
        }

        /// <summary>
        /// Tests <see cref="User"/> to <see cref="UserInfo"/> conversion.
        /// </summary>
        [Test]
        public void UserToUserInfoConversion()
        {
            var testUsers = this.GenerateTestUsers();
            for (int i = 0; i < testUsers.Count; i++)
            {
                var userInfo = new UserInfo(testUsers[i]);
                Assert.That(userInfo.ID, Is.EqualTo(testUsers[i].ID));
                Assert.That(userInfo.Username, Is.EqualTo(testUsers[i].Username));
                Assert.That(userInfo.FirstName, Is.EqualTo(testUsers[i].FirstName));
                Assert.That(userInfo.LastName, Is.EqualTo(testUsers[i].LastName));
                Assert.That(userInfo.EmailAddr, Is.EqualTo(testUsers[i].EmailAddr));
                Assert.That(userInfo.Telephone, Is.EqualTo(testUsers[i].Telephone));
            }
        }

        /// <summary>
        /// Tests the GetHashCode subroutine of <see cref="User"/>.
        /// </summary>
        [Test]
        public void UserHashesMatch()
        {
            var testUsers = this.GenerateTestUsers();
            for (int i = 0; i < testUsers.Count; i++)
            {
                Assert.That(testUsers[i].GetHashCode(), Is.EqualTo(testUsers[i].GetHashCode()));
                for (int j = 0; j < testUsers.Count; j++)
                {
                    if (i != j)
                    {
                        Assert.That(testUsers[i].GetHashCode(), Is.Not.EqualTo(testUsers[j].GetHashCode()));
                    }
                }
            }
        }

        /// <summary>
        /// Tests the Equal subroutine of <see cref="User"/>.
        /// </summary>
        [Test]
        public void UsersMatch()
        {
            var testUsers = this.GenerateTestUsers();
            for (int i = 0; i < testUsers.Count; i++)
            {
                Assert.That(testUsers[i].Equals(testUsers[i]));
                for (int j = 0; j < testUsers.Count; j++)
                {
                    if (i != j)
                    {
                        Assert.That(!testUsers[i].Equals(testUsers[j]));
                        Assert.That(!testUsers[j].Equals(testUsers[i]));
                    }
                }
            }
        }

        /// <summary>
        /// Test deleting a non-existent user.
        /// </summary>
        [Test]
        public void DeletingNonExistentUser()
        {
            this.ClearRepositories();
            Assert.Throws<UserManagementException>(() => this.UMan.DeleteUser(1));
        }

        /// <summary>
        /// Test attempting to delete a negative user ID.
        /// </summary>
        [Test]
        public void DeletingInvalidUserID()
        {
            Assert.Throws<UserManagementException>(() => this.UMan.DeleteUser(-1));
        }

        /// <summary>
        /// Test getting a non-existent user.
        /// </summary>
        [Test]
        public void GetNonExistentUser()
        {
            this.ClearRepositories();
            Assert.Throws<UserManagementException>(() => this.UMan.GetUserByID(1));
        }

        /// <summary>
        /// Test attempting to get a negative user ID.
        /// </summary>
        [Test]
        public void GetInvalidUserID()
        {
            Assert.Throws<UserManagementException>(() => this.UMan.GetUserByID(-1));
        }

        /// <summary>
        /// Test creating user with null as the username.
        /// </summary>
        [Test]
        public void UserRegistrationUsernameIsNull()
        {
            this.ClearRepositories();
            Assert.Throws<UserManagementException>(() => this.UMan.RegisterNewUser(1, null, "firstname", "lastname", "user@host", "123"));
        }

        /// <summary>
        /// Test creating user with null as the first name.
        /// </summary>
        [Test]
        public void UserRegistrationFirstNameIsNull()
        {
            this.ClearRepositories();
            Assert.Throws<UserManagementException>(() => this.UMan.RegisterNewUser(1, "username", null, "lastname", "user@host", "123"));
        }

        /// <summary>
        /// Test creating user with null as the last name.
        /// </summary>
        [Test]
        public void UserRegistrationLastNameIsNull()
        {
            this.ClearRepositories();
            Assert.Throws<UserManagementException>(() => this.UMan.RegisterNewUser(1, "username", "firstname", null, "user@host", "123"));
        }

        /// <summary>
        /// Attempts renaming a null user.
        /// </summary>
        [Test]
        public void RenameNullUser()
        {
            this.ClearRepositories();
            Assert.Throws<UserManagementException>(() => this.UMan.RenameUser(null, null, null));
        }

        /// <summary>
        /// Attempts renaming a non-existent user by ID.
        /// </summary>
        [Test]
        public void RenameNonExistentUserByID()
        {
            this.ClearRepositories();
            Assert.Throws<UserManagementException>(() => this.UMan.RenameUser(500, null, null));
        }
    }
}

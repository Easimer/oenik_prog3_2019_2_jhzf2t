﻿// <copyright file="IssueManagementTests.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using System.Linq;
    using Net.Easimer.Prog3.IssueTracker;
    using NUnit.Framework;

    /// <summary>
    /// Issue management tests.
    /// </summary>
    internal class IssueManagementTests : LogicTestEnvironment
    {
        /// <summary>
        /// Tests assigning an issue to a user.
        /// </summary>
        [Test]
        public void IssueAssigning()
        {
            this.ClearRepositories();
            var testIssue = this.GenerateTestIssues().First();
            var testUser = this.GenerateTestUsers().First();
            var testProject = this.GenerateProjects().First();
            this.RepoIssue.Entities.AddLast(testIssue);
            this.RepoUser.Entities.AddLast(testUser);
            this.RepoProj.Entities.AddLast(testProject);

            var res = this.IMan.AssignIssue(testIssue.ID, testUser.ID);

            Assert.AreEqual(res.ID, testIssue.ID);
            Assert.That(res.AssignedTo.HasValue);
            Assert.AreEqual(res.AssignedTo.Value, testUser.ID);

            Assert.That(testIssue.AssignedTo.HasValue);
            Assert.AreEqual(testIssue.AssignedTo.Value, testUser.ID);

            Assert.Throws<UserManagementException>(() => this.IMan.AssignIssue(testIssue.ID, 815162342));
            Assert.Throws<UserManagementException>(() => this.IMan.AssignIssue(testIssue.ID, -1));

            Assert.Throws<IssueManagementException>(() => this.IMan.AssignIssue(testIssue.ID + 1, testUser.ID));
            Assert.Throws<IssueManagementException>(() => this.IMan.AssignIssue(-1, testUser.ID));
        }

        /// <summary>
        /// Tests closing an issue.
        /// </summary>
        [Test]
        public void IssueClosing()
        {
            this.ClearRepositories();
            var testIssue = this.GenerateTestIssues().First();
            var testProject = this.GenerateProjects().First();
            this.RepoIssue.Entities.AddLast(testIssue);
            this.RepoProj.Entities.AddLast(testProject);

            var res = this.IMan.CloseIssue(testIssue.ID);

            Assert.That(res.Solved);
            Assert.That(testIssue.Solved);

            Assert.Throws<IssueManagementException>(() => this.IMan.CloseIssue(testIssue.ID + 1));
            Assert.Throws<IssueManagementException>(() => this.IMan.CloseIssue(-1));
        }

        /// <summary>
        /// Tests issue creation.
        /// </summary>
        [Test]
        public void IssueCreation()
        {
            this.ClearRepositories();
            var testUser = this.GenerateTestUsers().First();
            var testProject = this.GenerateProjects().First();
            this.RepoUser.Entities.AddLast(testUser);
            this.RepoProj.Entities.AddLast(testProject);

            var res = this.IMan.CreateNewIssue(testProject.ID, "Test Issue", testUser.ID);

            var ent = this.RepoIssue.Entities.First();

            Assert.AreEqual(res.Title, "Test Issue");
            Assert.AreEqual(ent.Title, "Test Issue");

            Assert.That(res.AssignedTo.HasValue);
            Assert.That(ent.AssignedTo.HasValue);

            Assert.That(res.Solved, Is.Not.True);
            Assert.That(ent.Solved, Is.Not.True);

            Assert.AreEqual(res.AssignedTo.Value, testUser.ID);
            Assert.AreEqual(ent.AssignedTo.Value, testUser.ID);
            Assert.AreEqual(res.Project, testProject.ID);
            Assert.AreEqual(ent.Project, testProject.ID);
        }

        /// <summary>
        /// Tests issue deletion.
        /// </summary>
        [Test]
        public void IssueDeletion()
        {
            this.ClearRepositories();
            var testIssue = this.GenerateTestIssues().First();
            var testProject = this.GenerateProjects().First();
            this.RepoIssue.Entities.AddLast(testIssue);
            this.RepoProj.Entities.AddLast(testProject);

            this.IMan.DeleteIssue(testIssue.ID);

            Assert.That(this.RepoIssue.Entities.Count() == 0);

            Assert.Throws<IssueManagementException>(() => this.IMan.DeleteIssue(testIssue.ID + 1));
            Assert.Throws<IssueManagementException>(() => this.IMan.DeleteIssue(-1));
        }

        /// <summary>
        /// Tests fetching all issues.
        /// </summary>
        [Test]
        public void FetchAll()
        {
            this.ClearRepositories();
            var testProjects = this.GenerateProjects();
            var testIssues = this.GenerateTestIssues();
            foreach (var proj in testProjects)
            {
                this.RepoProj.Entities.AddLast(proj);
            }

            foreach (var issue in testIssues)
            {
                this.RepoIssue.Entities.AddLast(issue);
            }

            var allIssues = this.IMan.GetAllIssues();

            Assert.AreEqual(allIssues.Count(), testIssues.Count());

            Assert.That(testIssues.All(o => allIssues.Any(c => o.Equals(c))));
        }

        /// <summary>
        /// Tests IIssueManagement::GetAllIssues(int) with valid project IDs.
        /// </summary>
        /// <param name="projectID">Project ID.</param>
        [TestCase(1)]
        [TestCase(2)]
        public void FetchAllProjectID(int projectID)
        {
            this.ClearRepositories();
            var testProjects = this.GenerateProjects();
            var testIssues = this.GenerateTestIssues();
            foreach (var proj in testProjects)
            {
                this.RepoProj.Entities.AddLast(proj);
            }

            foreach (var issue in testIssues)
            {
                this.RepoIssue.Entities.AddLast(issue);
            }

            var allIssues = this.IMan.GetAllIssues(projectID);

            Assert.AreEqual(allIssues.Count(), testIssues.Where(i => i.Project == projectID).Count());
            Assert.That(allIssues.All(i => testIssues.Contains(i)));
            Assert.That(allIssues.All(i => testIssues.Exists(ti => ti.Equals(i))));
        }

        /// <summary>
        /// Tests IIssueManagement::GetAllIssues(int) with an invalid project ID.
        /// </summary>
        public void FetchAllInvalidProjectID()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.IMan.GetAllIssues(-1));
        }

        /// <summary>
        /// Tests IIssueManagement::GetAllIssues(int) with a non-existent project ID.
        /// </summary>
        public void FetchAllNotExistentProjectID()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.IMan.GetAllIssues(1));
        }

        /// <summary>
        /// Tests IIssueManagement::GetByID(int) with a valid ID.
        /// </summary>
        /// <param name="issueID">Issue ID.</param>
        [TestCase(1)]
        [TestCase(2)]
        public void FetchByID(int issueID)
        {
            this.ClearRepositories();
            var testProjects = this.GenerateProjects();
            var testIssues = this.GenerateTestIssues();
            foreach (var proj in testProjects)
            {
                this.RepoProj.Entities.AddLast(proj);
            }

            foreach (var issue in testIssues)
            {
                this.RepoIssue.Entities.AddLast(issue);
            }

            var res = this.IMan.GetById(issueID);

            Assert.That(testIssues.Any(ti => ti.Equals(res)));
            Assert.That(res, Is.EqualTo(testIssues.Where(ti => ti.ID == issueID).First()));
        }

        /// <summary>
        /// Tests IIssueManagement::GetByID(int) with an invalid ID.
        /// </summary>
        [Test]
        public void FetchByInvalidID()
        {
            this.ClearRepositories();
            Assert.Throws<IssueManagementException>(() => this.IMan.GetById(-1));
        }

        /// <summary>
        /// Tests IIssueManagement::GetByID(int) with a non-existent ID.
        /// </summary>
        [Test]
        public void FetchByNonExistentID()
        {
            this.ClearRepositories();
            Assert.Throws<IssueManagementException>(() => this.IMan.GetById(999));
        }

        /// <summary>
        /// Tests issue creation, assigning it to a non-existent user at creation time.
        /// </summary>
        [Test]
        public void IssueCreationNonExistentUser()
        {
            this.ClearRepositories();
            var testProject = this.GenerateProjects().First();
            this.RepoProj.Entities.AddLast(testProject);

            Assert.Throws<UserManagementException>(() => this.IMan.CreateNewIssue(testProject.ID, "Test Issue", 500));
        }

        /// <summary>
        /// Tests issue creation for a non-existent project.
        /// </summary>
        [Test]
        public void IssueCreationNonExistentProject()
        {
            this.ClearRepositories();
            var testUser = this.GenerateTestUsers().First();
            this.RepoUser.Entities.AddLast(testUser);

            Assert.Throws<ProjectManagementException>(() => this.IMan.CreateNewIssue(500, "Test Issue", testUser.ID));
        }

        /// <summary>
        /// Tests issue creation using a repository that won't create the issue.
        /// </summary>
        [Test]
        public void IssueCreationFaultyRepo()
        {
            this.RepoIssue.FaultyMode = FakeIssuesRepository.FaultyModeFlags.Create;
            Assert.Throws<IssueManagementException>(() => this.IssueCreation());
            this.RepoIssue.FaultyMode = FakeIssuesRepository.FaultyModeFlags.None;
        }

        /// <summary>
        /// Tests issue deletion using a repository that won't delete the issue.
        /// </summary>
        [Test]
        public void IssueDeletionFaulyRepo()
        {
            this.ClearRepositories();
            var testIssue = this.GenerateTestIssues().First();
            var testUser = this.GenerateTestUsers().First();
            var testProject = this.GenerateProjects().First();
            this.RepoIssue.Entities.AddLast(testIssue);
            this.RepoUser.Entities.AddLast(testUser);
            this.RepoProj.Entities.AddLast(testProject);

            this.RepoIssue.FaultyMode = FakeIssuesRepository.FaultyModeFlags.Delete;
            Assert.Throws<IssueManagementException>(() => this.IMan.DeleteIssue(testIssue.ID));
            this.RepoIssue.FaultyMode = FakeIssuesRepository.FaultyModeFlags.None;
        }

        /// <summary>
        /// Tests searching for issues across all projects.
        /// </summary>
        /// <param name="filter">Filter string.</param>
        [TestCase("1")]
        [TestCase("Issue")]
        public void SearchIssuesAcrossAllProjects(string filter)
        {
            this.ClearRepositories();
            var testProjects = this.GenerateProjects();
            var testIssues = this.GenerateTestIssues();
            foreach (var proj in testProjects)
            {
                this.RepoProj.Entities.AddLast(proj);
            }

            foreach (var issue in testIssues)
            {
                this.RepoIssue.Entities.AddLast(issue);
            }

            var res = this.IMan.SearchForIssues(filter, null).ToArray();
            Assert.That(res.Length, Is.GreaterThan(0));
            Assert.That(res.All(i => i.Title.Contains(filter)));
            Assert.That(testIssues.Where(ti => !res.Contains(ti)).All(i => !i.Title.Contains(filter)));
        }

        /// <summary>
        /// Tests searching for issues in one project.
        /// </summary>
        /// <param name="filter">Filter string.</param>
        /// <param name="projectID">Project ID.</param>
        /// <param name="nHits">Number of hits.</param>
        [TestCase("1", 1, 1)]
        [TestCase("Issue", 1, 2)]
        public void SearchIssuesInOneProject(string filter, int projectID, int nHits)
        {
            this.ClearRepositories();
            var testProjects = this.GenerateProjects();
            var testIssues = this.GenerateTestIssues();
            foreach (var proj in testProjects)
            {
                this.RepoProj.Entities.AddLast(proj);
            }

            foreach (var issue in testIssues)
            {
                this.RepoIssue.Entities.AddLast(issue);
            }

            var res = this.IMan.SearchForIssues(filter, projectID).ToArray();
            Assert.That(res.Length, Is.EqualTo(nHits));
            Assert.That(res.All(i => i.Title.Contains(filter)));
            Assert.That(res.All(i => i.Project == projectID));
            Assert.That(testIssues.Where(ti => !res.Contains(ti)).All(i => !i.Title.Contains(filter)));
        }
    }
}

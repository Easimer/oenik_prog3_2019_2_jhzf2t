﻿// <copyright file="ProjectManagementTests.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;
    using Net.Easimer.Prog3.IssueTracker;
    using Net.Easimer.Prog3.IssueTracker.Impl;
    using NUnit.Framework;

    /// <summary>
    /// Project management tests.
    /// </summary>
    [TestFixture]
    internal sealed class ProjectManagementTests : LogicTestEnvironment
    {
        /// <summary>
        /// Test project creation.
        /// </summary>
        [Test]
        public void ProjectCreate()
        {
            var testProjects = this.GenerateProjects();
            this.ClearRepositories();
            foreach (var testProj in testProjects)
            {
                this.PMan.CreateProject(testProj.ID, testProj.Name, testProj.Homepage, testProj.Owner, testProj.Parent, testProj.CreationDate);
                bool eq = this.RepoProj.Entities.First.Value.Equals(testProj);
                Assert.AreEqual(this.RepoProj.Entities.Last.Value, testProj);
            }
        }

        /// <summary>
        /// Test project deletion.
        /// </summary>
        [Test]
        public void ProjectDelete()
        {
            var testProjects = this.GenerateProjects();
            this.ClearRepositories();

            foreach (var testProject in testProjects)
            {
                this.RepoProj.Entities.AddLast(testProject);
            }

            for (int i = 0; i < testProjects.Count; i++)
            {
                Assert.That(this.RepoProj.Entities.Count, Is.EqualTo(testProjects.Count - i));
                this.PMan.DeleteByID(testProjects[i].ID);
                Assert.That(this.RepoProj.Entities.Count, Is.EqualTo(testProjects.Count - i - 1));
            }
        }

        /// <summary>
        /// Test project fetching.
        /// </summary>
        [Test]
        public void ProjectFetchAll()
        {
            var testProjects = this.GenerateProjects();
            this.ClearRepositories();
            foreach (var testProj in testProjects)
            {
                this.RepoProj.Entities.AddLast(testProj);
            }

            var result = this.PMan.GetAllProjects().ToList();
            Assert.That(result.Count, Is.EqualTo(testProjects.Count));
            ////testProjects.ForEach(x => Assert.That(result.Any(y => y.Equals(new ProjectInfo(this.RepoProj, this.RepoUser, x)))));
            for (int proj = 0; proj < testProjects.Count; proj++)
            {
                bool ret = false;
                for (int res = 0; res < testProjects.Count; res++)
                {
                    if (testProjects[proj].Equals(result[res]))
                    {
                        ret = true;
                    }
                }

                Assert.That(ret);
            }
        }

        /// <summary>
        /// Test finding project by ID.
        /// </summary>
        [Test]
        public void ProjectFindById()
        {
            var testProjects = this.GenerateProjects();
            this.ClearRepositories();
            foreach (var testProj in testProjects)
            {
                this.RepoProj.Entities.AddLast(testProj);
            }

            for (int i = 0; i < testProjects.Count; i++)
            {
                var res = this.PMan.GetByID(testProjects[i].ID);
                Assert.That(res, Is.Not.EqualTo(null));
                Assert.That(res.ID, Is.EqualTo(testProjects[i].ID));
                Assert.That(res, Is.EqualTo(new ProjectInfo(this.RepoProj, this.RepoUser, testProjects[i])));
            }
        }

        /// <summary>
        /// Tests <see cref="Project"/> to <see cref="ProjectInfo"/> conversion.
        /// </summary>
        [Test]
        public void ProjectToProjectInfoConversion()
        {
            this.ClearRepositories();
            var testProjs = this.GenerateProjects();
            for (int i = 0; i < testProjs.Count; i++)
            {
                var projInfo = new ProjectInfo(this.RepoProj, this.RepoUser, testProjs[i]);
                Assert.AreEqual(projInfo.ID, testProjs[i].ID);
                Assert.AreEqual(projInfo.Name, testProjs[i].Name);
                Assert.AreEqual(projInfo.Homepage, testProjs[i].Homepage);
                Assert.AreEqual(projInfo.CreationDate, testProjs[i].CreationDate);

                if (testProjs[i].Owner.HasValue)
                {
                    Assert.AreEqual(projInfo.OwnerRef.ID, testProjs[i].Owner);
                }
                else
                {
                    Assert.That(projInfo.Owner, Is.EqualTo(null));
                }

                if (testProjs[i].Parent.HasValue)
                {
                    Assert.AreEqual(projInfo.ParentRef.ID, testProjs[i].Parent);
                }
                else
                {
                    Assert.That(projInfo.ParentRef, Is.EqualTo(null));
                }

                this.RepoProj.Entities.AddLast(testProjs[i]);
            }
        }

        /// <summary>
        /// Tests the GetHashCode subroutine of <see cref="Project"/>.
        /// </summary>
        [Test]
        public void ProjectHashesMatch()
        {
            var testProjs = this.GenerateProjects();
            for (int i = 0; i < testProjs.Count; i++)
            {
                Assert.AreEqual(testProjs[i].GetHashCode(), testProjs[i].GetHashCode());
                for (int j = 0; j < testProjs.Count; j++)
                {
                    if (i != j)
                    {
                        Assert.AreNotEqual(testProjs[i].GetHashCode(), testProjs[j].GetHashCode());
                    }
                }
            }
        }

        /// <summary>
        /// Tests the Equal subroutine of <see cref="Project"/>.
        /// </summary>
        [Test]
        public void ProjectsMatch()
        {
            var testProjs = this.GenerateProjects();
            for (int i = 0; i < testProjs.Count; i++)
            {
                Assert.That(testProjs[i].Equals(testProjs[i]));
                for (int j = 0; j < testProjs.Count; j++)
                {
                    if (i != j)
                    {
                        Assert.That(!testProjs[i].Equals(testProjs[j]));
                        Assert.That(!testProjs[j].Equals(testProjs[i]));
                    }
                }
            }
        }

        /// <summary>
        /// Tests IProjectManagement::Rename.
        /// </summary>
        [Test]
        public void ProjectRename()
        {
            FakeProjectsRepository projects;
            Mock<IDataAccess> dac;
            ProjectManager pman;

            List<Project> testProjects = this.GenerateProjects();

            projects = new FakeProjectsRepository();
            dac = new Mock<IDataAccess>();
            dac.Setup(x => x.GetProjectsRepository()).Returns(projects);
            pman = new ProjectManager(dac.Object);

            var proj = new Project();
            proj.Name = "old";
            proj.ID = 1;
            projects.Entities.AddLast(proj);

            pman.Rename(new ProjectInfo(this.RepoProj, this.RepoUser, proj), "new");

            Assert.AreEqual(proj.Name, "new");
        }

        /// <summary>
        /// Tests IProjectManagement::SetOwner.
        /// </summary>
        [Test]
        public void ProjectSetOwnerOnce()
        {
            this.ClearRepositories();
            var pinf = this.PMan.CreateProject(1, "Test", "old", null);
            var user0 = this.UMan.RegisterNewUser(1, "test0", "Zero", "Test");
            var user1 = this.UMan.RegisterNewUser(2, "test1", "One", "Test");

            this.PMan.SetOwner(pinf, user0);
            Assert.AreEqual(this.RepoProj.Entities.First().Owner, user0.ID);
        }

        /// <summary>
        /// Tests IProjectManagement::SetOwner.
        /// </summary>
        [Test]
        public void ProjectSetOwnerThenOverwrite()
        {
            this.ClearRepositories();
            var pinf = this.PMan.CreateProject(1, "Test", "old", null);
            var user0 = this.UMan.RegisterNewUser(1, "test0", "Zero", "Test");
            var user1 = this.UMan.RegisterNewUser(2, "test1", "One", "Test");

            this.PMan.SetOwner(pinf, user0);
            Assert.AreEqual(this.RepoProj.Entities.First().Owner, user0.ID);

            this.PMan.SetOwner(pinf, user1);
            Assert.AreEqual(this.RepoProj.Entities.First().Owner, user1.ID);
        }

        /// <summary>
        /// Tests IProjectManagement::SetHomepage.
        /// </summary>
        /// <param name="newHomepage">New homepage URL.</param>
        [TestCase("about:blank")]
        [TestCase("https://example.com/")]
        [TestCase(null)]
        public void ProjectSetHomepage(string newHomepage)
        {
            this.ClearRepositories();

            var pinf = this.PMan.CreateProject(1, "Test", "old", null);

            this.PMan.SetHomepage(pinf, newHomepage);
            Assert.AreEqual(this.RepoProj.Entities.First().Homepage, newHomepage);
        }

        /// <summary>
        /// Tests for the case when the client attempts to create a new project owned by itself
        /// but the it doesn't have an entry in the Users table.
        /// </summary>
        [Test]
        public void CreatingProjectOwnedByNonExistentUserID()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.PMan.CreateProject(1, "Test", "test", 1000));
        }

        /// <summary>
        /// Tests setting the owner of a project using a malformed ProjectInfo descriptor.
        /// </summary>
        [Test]
        public void SetOwnerWithBadProjectInfo()
        {
            this.ClearRepositories();

            var forgedProj = new ProjectInfo();
            forgedProj.ID = 1;

            Assert.Throws<ProjectManagementException>(() => this.PMan.SetOwner(forgedProj, new UserInfo()));
        }

        /// <summary>
        /// Tests setting the owner of a project using a null descriptor.
        /// </summary>
        [Test]
        public void SetOwnerNullProject()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.PMan.SetOwner(null, new UserInfo()));
        }

        /// <summary>
        /// Tests setting the homepage of a project using a malformed ProjectInfo descriptor.
        /// </summary>
        [Test]
        public void SetHomepageWithBadProjectInfo()
        {
            this.ClearRepositories();

            var forgedProj = new ProjectInfo();
            forgedProj.ID = 1;

            Assert.Throws<ProjectManagementException>(() => this.PMan.SetHomepage(forgedProj, "about:blank"));
        }

        /// <summary>
        /// Tests setting the owner of a project using a malformed ProjectInfo descriptor.
        /// </summary>
        [Test]
        public void RenameWithBadProjectInfo()
        {
            this.ClearRepositories();

            var forgedProj = new ProjectInfo();
            forgedProj.ID = 1;

            Assert.Throws<ProjectManagementException>(() => this.PMan.Rename(forgedProj, "new name"));
        }

        /// <summary>
        /// Tests setting the owner of a project using a null descriptor.
        /// </summary>
        [Test]
        public void RenameNullProject()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.PMan.Rename(null, "new name"));
        }

        /// <summary>
        /// Tests getting a project by an non-existent ID.
        /// </summary>
        [Test]
        public void GetProjectByIdNonExistent()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.PMan.GetByID(500));
        }

        /// <summary>
        /// Tests deleting a project by an non-existent ID.
        /// </summary>
        [Test]
        public void DeleteProjectByIdNonExistent()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.PMan.DeleteByID(500));
        }

        /// <summary>
        /// Tests trying to set the homepage with a null handle.
        /// </summary>
        [Test]
        public void SetHomepageForNullProject()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.PMan.SetHomepage(null, "asd"));
        }

        /// <summary>
        /// Tests clearing the owner of a project.
        /// </summary>
        [Test]
        public void ClearOwner()
        {
            this.ClearRepositories();
            var p = new Project();
            p.ID = 1;
            p.Owner = null;
            this.RepoProj.Create(p);
            var pinf = new ProjectInfo(this.RepoProj, this.RepoUser, p);

            this.PMan.SetOwner(pinf, null);
            Assert.That(this.RepoProj.Entities.First().Owner.HasValue, Is.False);
        }

        /// <summary>
        /// Tests trying to create a project without a name.
        /// </summary>
        [Test]
        public void CreateProjectWithoutName()
        {
            this.ClearRepositories();

            Assert.Throws<ProjectManagementException>(() => this.PMan.CreateProject(1, null, string.Empty, null));
        }
    }
}

﻿// <copyright file="BaseRepositoryImpl.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Base repository implementation.
    /// </summary>
    /// <typeparam name="T">Type of entity.</typeparam>
    internal abstract class BaseRepositoryImpl<T> : IRepository<T>
        where T : class, IEntityWithID
    {
        private DbSet<T> refDbSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepositoryImpl{T}"/> class.
        /// </summary>
        /// <param name="refDbSet">Reference to the DbSet in the model.</param>
        public BaseRepositoryImpl(DbSet<T> refDbSet)
        {
            this.refDbSet = refDbSet;
        }

        /// <inheritdoc/>
        public int Create(params T[] instance)
        {
            this.refDbSet.AddRange(instance);
            return this.Flush();
        }

        /// <inheritdoc/>
        public int Delete(params T[] instance)
        {
            this.refDbSet.RemoveRange(instance);
            return this.Flush();
        }

        /// <inheritdoc/>
        public T GetById(int id)
        {
            T ret = null;

            var res = from entity in this.GetByIdJoined() where entity.ID == id select entity;
            if (res.Count() > 0)
            {
                ret = res.First();
            }

            return ret;
        }

        /// <inheritdoc/>
        public IQueryable<T> ReadAll()
        {
            return this.ReadAllJoined();
        }

        /// <summary>
        /// How to save changes to the database.
        /// </summary>
        /// <returns>Number of rows affected.</returns>
        protected abstract int Flush();

        /// <summary>
        /// Override this to provide the table joining information for the ReadAll() operation.
        /// </summary>
        /// <returns>A queryable object.</returns>
        protected virtual IQueryable<T> ReadAllJoined()
        {
            return this.refDbSet;
        }

        /// <summary>
        /// Override this to provide the table joining information for the GetById() operation.
        /// </summary>
        /// <returns>A queryable object.</returns>
        protected virtual IQueryable<T> GetByIdJoined()
        {
            return this.refDbSet;
        }
    }
}

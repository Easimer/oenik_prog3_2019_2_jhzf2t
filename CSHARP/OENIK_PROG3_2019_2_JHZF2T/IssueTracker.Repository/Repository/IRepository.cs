﻿// <copyright file="IRepository.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository interface.
    /// </summary>
    /// <typeparam name="TEnt">Entity type.</typeparam>
    public interface IRepository<TEnt>
        where TEnt : class
    {
        /// <summary>
        /// Adds new entities into the repository.
        /// </summary>
        /// <param name="instance">Entity instances.</param>
        /// <returns>Number of created entities.</returns>
        int Create(params TEnt[] instance);

        /// <summary>
        /// Delete entities.
        /// </summary>
        /// <param name="instance">Entity instances.</param>
        /// <returns>Number of deleted entities.</returns>
        int Delete(params TEnt[] instance);

        /// <summary>
        /// Fetch all entities.
        /// </summary>
        /// <returns>An IQueryable containing all entities.</returns>
        IQueryable<TEnt> ReadAll();

        /// <summary>
        /// Get and entity by it's ID.
        /// </summary>
        /// <param name="id">Entity ID.</param>
        /// <returns>Reference to the entity (or NULL if it doesn't exist).</returns>
        TEnt GetById(int id);
    }
}

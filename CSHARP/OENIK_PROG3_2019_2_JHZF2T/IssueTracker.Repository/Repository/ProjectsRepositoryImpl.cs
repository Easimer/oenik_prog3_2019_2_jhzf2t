﻿// <copyright file="ProjectsRepositoryImpl.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database.Repository
{
    using System.Linq;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Projects repository implementation.
    /// </summary>
    internal class ProjectsRepositoryImpl : BaseRepositoryImpl<Project>, IProjects
    {
        private IIssueTrackerModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectsRepositoryImpl"/> class.
        /// </summary>
        /// <param name="model">Issue tracker model instance.</param>
        public ProjectsRepositoryImpl(IIssueTrackerModel model)
            : base(model.Projects)
        {
            this.model = model;
        }

        /// <inheritdoc/>
        protected override int Flush()
        {
            return this.model.SaveChanges();
        }
    }
}

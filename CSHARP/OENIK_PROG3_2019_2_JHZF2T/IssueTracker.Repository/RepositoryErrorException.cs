﻿// <copyright file="RepositoryErrorException.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database
{
    using System;

    /// <summary>
    /// Repository error exception.
    /// </summary>
    public class RepositoryErrorException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryErrorException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        public RepositoryErrorException(string message)
            : base(message)
        {
        }
    }
}

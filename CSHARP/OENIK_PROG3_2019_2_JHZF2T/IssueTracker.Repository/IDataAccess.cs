﻿// <copyright file="IDataAccess.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database
{
    using Net.Easimer.Prog3.Database.Repository;

    /// <summary>
    /// Data access interface.
    /// </summary>
    public interface IDataAccess
    {
        /// <summary>
        /// Gets the users repository.
        /// </summary>
        /// <returns>IUsers instance.</returns>
        IUsers GetUsersRepository();

        /// <summary>
        /// Gets the projects repository.
        /// </summary>
        /// <returns>IProjects instance.</returns>
        IProjects GetProjectsRepository();

        /// <summary>
        /// Gets the issues repository.
        /// </summary>
        /// <returns>IISsues instance.</returns>
        IIssues GetIssuesRepository();

        /// <summary>
        /// Flush changes.
        /// </summary>
        void FlushChanges();
    }
}

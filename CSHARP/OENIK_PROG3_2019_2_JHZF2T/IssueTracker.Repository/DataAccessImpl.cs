﻿// <copyright file="DataAccessImpl.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Database
{
    using Net.Easimer.Prog3.Database.Data;
    using Net.Easimer.Prog3.Database.Repository;

    /// <summary>
    /// Data access implementation.
    /// </summary>
    public class DataAccessImpl : IDataAccess
    {
        private IssueTrackerModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataAccessImpl"/> class.
        /// </summary>
        public DataAccessImpl()
        {
            this.model = new IssueTrackerModel();
        }

        /// <inheritdoc/>
        public void FlushChanges()
        {
            this.model.SaveChanges();
        }

        /// <inheritdoc/>
        public IProjects GetProjectsRepository()
        {
            return new ProjectsRepositoryImpl(this.model);
        }

        /// <inheritdoc/>
        public IUsers GetUsersRepository()
        {
            return new UsersRepositoryImpl(this.model);
        }

        /// <inheritdoc/>
        public IIssues GetIssuesRepository()
        {
            return new IssuesRepositoryImpl(this.model);
        }
    }
}

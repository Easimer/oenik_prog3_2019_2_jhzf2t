﻿// <copyright file="IProjectManagement.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;
    using System.Collections.Generic;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Project management interface.
    /// </summary>
    public interface IProjectManagement
    {
        /// <summary>
        /// Fetch the list of all projects.
        /// </summary>
        /// <returns>Enumerable of all projects.</returns>
        /// <exception cref="ProjectManagementException">Thrown on error.</exception>
        IEnumerable<ProjectInfo> GetAllProjects();

        /// <summary>
        /// Get the project by ID.
        /// </summary>
        /// <param name="id">Project ID.</param>
        /// <returns>ProjectInfo instance or null.</returns>
        /// <exception cref="ProjectManagementException">Thrown when project #id doesn't exist.</exception>
        ProjectInfo GetByID(int id);

        /// <summary>
        /// Delete a project by ID and every single one of it's issues.
        /// </summary>
        /// <param name="id">Project ID.</param>
        /// <returns>A value indicating whether the deletion succeeded.</returns>
        /// <exception cref="ProjectManagementException">Thrown when project #id doesn't exist.</exception>
        bool DeleteByID(int id);

        /// <summary>
        /// Create a new project, optionally specifying the owner user's ID.
        /// </summary>
        /// <param name="id">Project ID.</param>
        /// <param name="name">Project name.</param>
        /// <param name="homepage">Project homepage.</param>
        /// <param name="ownerID">Project owner's ID.</param>
        /// <returns>Project information or null on error.</returns>
        /// <exception cref="ProjectManagementException">Thrown if <paramref name="ownerID"/> refers to a non-existent user or <paramref name="name"/>  was null.</exception>
        ProjectInfo CreateProject(int id, string name, string homepage, int? ownerID);

        /// <summary>
        /// Create a new project, optionally specifying the owner user's ID.
        /// </summary>
        /// <param name="id">Project ID.</param>
        /// <param name="name">Project name.</param>
        /// <param name="homepage">Project homepage.</param>
        /// <param name="ownerID">Project owner's ID.</param>
        /// <param name="parentID">ID of the project's parent.</param>
        /// <param name="creationDate">Date of creation.</param>
        /// <returns>Project information or null on error.</returns>
        ProjectInfo CreateProject(int id, string name, string homepage, int? ownerID, int? parentID, DateTime creationDate);

        /// <summary>
        /// Rename a project.
        /// </summary>
        /// <param name="prj">Project information.</param>
        /// <param name="newName">New name.</param>
        void Rename(ProjectInfo prj, string newName);

        /// <summary>
        /// Set a project's homepage.
        /// </summary>
        /// <param name="prj">Project information.</param>
        /// <param name="homepage">New homepage URL or null to set to none.</param>
        void SetHomepage(ProjectInfo prj, string homepage);

        /// <summary>
        /// Set a project's owner.
        /// </summary>
        /// <param name="prj">Project information.</param>
        /// <param name="usr">User information.</param>
        void SetOwner(ProjectInfo prj, UserInfo usr);
    }
}

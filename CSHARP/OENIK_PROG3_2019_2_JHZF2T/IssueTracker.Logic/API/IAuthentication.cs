﻿// <copyright file="IAuthentication.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;

    /// <summary>
    /// User authentication.
    ///
    /// NOTE(danielm): doesn't do any real AAA because we're directly
    /// connecting to the DB. This interface merely provides a way to get the
    /// user ID belonging to our username. In a real application I'd have an
    /// actually working authentication system in place.
    /// </summary>
    public interface IAuthentication
    {
        /// <summary>
        /// Authenticates the provided credentials and returns the user ID.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        /// <exception cref="AuthenticationException">Thrown when the credentials are invalid.</exception>
        /// <returns>The user ID associated with the credentials.</returns>
        int Authenticate(string username, string password);
    }

    /// <summary>
    /// Authentication error.
    /// </summary>
    [Serializable]
    public class AuthenticationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationException"/> class.
        /// </summary>
        public AuthenticationException()
            : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationException"/> class.
        /// </summary>
        /// <param name="msg">The error message.</param>
        public AuthenticationException(string msg)
            : this(msg, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationException"/> class.
        /// </summary>
        /// <param name="msg">The error message.</param>
        /// <param name="innerException">Inner exception.</param>
        public AuthenticationException(string msg, Exception innerException)
            : base(msg, innerException)
        {
        }
    }
}

﻿// <copyright file="IWebProg3.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    /// <summary>
    /// Provides access to a Prog3 backend server.
    /// </summary>
    public interface IWebProg3
    {
        /// <summary>
        /// Authenticate user credentials.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        /// <returns>The user ID if the credentials were correct.</returns>
        /// <exception cref="AuthenticationException">Thrown when the credentials are incorrect.</exception>
        int Authenticate(string username, string password);

        /// <summary>
        /// Return the list of the newest user complaints.
        /// </summary>
        /// <returns>A list of the newest user complaints.</returns>
        IEnumerable<Complaint> GetComplaints();
    }

    /// <summary>
    /// A structure describing a user complaint.
    /// </summary>
    [XmlTypeAttribute(AnonymousType = true)]
    public class Complaint
    {
        /// <summary>
        /// Gets or sets the author of the complaint.
        /// </summary>
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string Author { get; set; }

        /// <summary>
        /// Gets or sets the contents of the complaint.
        /// </summary>
        [XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string Contents { get; set; }
    }
}

﻿// <copyright file="ProjectManagementException.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Project management exception.
    /// </summary>
    public class ProjectManagementException : BaseManagementException<ProjectInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectManagementException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="project">Project related to the incident.</param>
        public ProjectManagementException(string message = "", ProjectInfo project = null)
            : base(message, project)
        {
        }
    }
}

﻿// <copyright file="IssueInfo.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;
    using Net.Easimer.Prog3.Database.Repository;

    /// <summary>
    /// Issue information.
    /// </summary>
    public class IssueInfo : Issue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IssueInfo"/> class.
        /// </summary>
        public IssueInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IssueInfo"/> class.
        /// </summary>
        /// <param name="projects">IProjects instance.</param>
        /// <param name="users">IUsers instance.</param>
        /// <param name="ent">Entity instance.</param>
        public IssueInfo(IProjects projects, IUsers users, Issue ent)
            : base(ent)
        {
            this.ProjectRef = new ProjectInfo(projects, users, projects.GetById(this.Project));

            if (this.AssignedTo.HasValue)
            {
                this.AssignedToRef = new UserInfo(users.GetById(this.AssignedTo.Value));
            }
        }

        /// <summary>
        /// Gets or sets the reference to the project this issue belongs to.
        /// </summary>
        [IgnoredByEditor]
        public new ProjectInfo ProjectRef { get; set; }

        /// <summary>
        /// Gets or sets the reference to the user this issue was assigned to.
        /// </summary>
        [EditorDescription("Assign to a user")]
        public new UserInfo AssignedToRef { get; set; }
    }
}

﻿// <copyright file="UserManagementException.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;

    /// <summary>
    /// User management exception.
    /// </summary>
    public class UserManagementException : BaseManagementException<UserInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagementException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="project">Project related to the incident.</param>
        public UserManagementException(string message = "", UserInfo project = null)
            : base(message, project)
        {
        }
    }
}

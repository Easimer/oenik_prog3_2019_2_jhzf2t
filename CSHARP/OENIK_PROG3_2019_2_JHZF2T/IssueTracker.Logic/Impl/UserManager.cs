﻿// <copyright file="UserManager.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker.Impl
{
    using System;
    using System.Collections.Generic;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// User manager implementation.
    /// </summary>
    internal class UserManager : IUserManagement
    {
        private IDataAccess dataAccess;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManager"/> class.
        /// </summary>
        /// <param name="dataAccess">Data access interface.</param>
        public UserManager(IDataAccess dataAccess)
        {
            this.dataAccess = dataAccess;
        }

        /// <inheritdoc/>
        public bool DeleteUser(int id)
        {
            bool ret = false;
            User dbd = new User();

            if (id >= 0)
            {
                var users = this.dataAccess.GetUsersRepository();
                dbd = users.GetById(id);
                if (dbd != null)
                {
                    try
                    {
                        ret = users.Delete(dbd) > 0;
                    }
                    catch (AggregateException ex)
                    {
                        // Forward errors
                        throw ex;
                    }
                }
                else
                {
                    throw new UserManagementException(string.Format("User ID #{0} doesn't exist!", id));
                }
            }
            else
            {
                throw new UserManagementException(string.Format("User ID #{0} is invalid", id));
            }

            return ret;
        }

        /// <inheritdoc/>
        public IEnumerable<UserInfo> GetAllUsers()
        {
            var ret = new List<UserInfo>();
            var users = this.dataAccess.GetUsersRepository();
            var user_list = users.ReadAll();
            foreach (var user in user_list)
            {
                ret.Add(new UserInfo(user));
            }

            return ret;
        }

        /// <inheritdoc/>
        public UserInfo GetUserByID(int id)
        {
            UserInfo ret = null;
            var users = this.dataAccess.GetUsersRepository();
            var userEnt = users.GetById(id);

            if (userEnt != null)
            {
                ret = new UserInfo(userEnt);
            }
            else
            {
                throw new UserManagementException(string.Format("User #{0} doesn't exist", id));
            }

            return ret;
        }

        /// <inheritdoc/>
        public UserInfo RegisterNewUser(int id, string username, string firstName, string lastName, string email = null, string telephone = null)
        {
            UserInfo ret = null;
            int res = 0;
            var users = this.dataAccess.GetUsersRepository();
            var user = new User();
            user.ID = id;
            user.Username = username;
            user.FirstName = firstName;
            user.LastName = lastName;
            user.EmailAddr = email;
            user.Telephone = telephone;

            try
            {
                res = users.Create(user);
            }
            catch (Exception ex)
            {
                throw new UserManagementException(ex.Message, ret);
            }

            if (res != 0)
            {
                ret = new UserInfo(user);
            }

            return ret;
        }

        /// <inheritdoc/>
        public UserInfo RenameUser(UserInfo subject, string firstName, string lastName)
        {
            UserInfo ret = null;

            if (subject != null)
            {
                ret = this.RenameUser(subject.ID, firstName, lastName);
            }
            else
            {
                throw new UserManagementException("Attempted to rename nobody!");
            }

            return ret;
        }

        /// <inheritdoc/>
        public UserInfo RenameUser(int userID, string firstName, string lastName)
        {
            UserInfo ret = null;

            var user = this.dataAccess.GetUsersRepository().GetById(userID);
            if (user != null)
            {
                if (firstName != null)
                {
                    user.FirstName = firstName;
                }

                if (lastName != null)
                {
                    user.LastName = lastName;
                }

                this.dataAccess.FlushChanges();
                ret = new UserInfo(user);
            }
            else
            {
                throw new UserManagementException(
                    string.Format("User #{0} doesn't exist or is an invalid identifier!", userID));
            }

            return ret;
        }
    }
}

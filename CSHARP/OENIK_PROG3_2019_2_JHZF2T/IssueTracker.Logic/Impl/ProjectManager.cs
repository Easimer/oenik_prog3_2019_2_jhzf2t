﻿// <copyright file="ProjectManager.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker.Impl
{
    using System;
    using System.Collections.Generic;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;

    /// <summary>
    /// Project manager.
    /// </summary>
    internal class ProjectManager : IProjectManagement
    {
        private IDataAccess dataAccess;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectManager"/> class.
        /// </summary>
        /// <param name="dataAccess">Data access interface.</param>
        public ProjectManager(IDataAccess dataAccess)
        {
            this.dataAccess = dataAccess;
        }

        /// <inheritdoc/>
        public ProjectInfo CreateProject(int id, string name, string homepage, int? ownerID)
        {
            return this.CreateProject(id, name, homepage, ownerID, null, DateTime.Now);
        }

        /// <inheritdoc/>
        public IEnumerable<ProjectInfo> GetAllProjects()
        {
            var ret = new List<ProjectInfo>();
            var repoProj = this.dataAccess.GetProjectsRepository();
            var repoUser = this.dataAccess.GetUsersRepository();
            var project_list = repoProj.ReadAll();
            foreach (var project in project_list)
            {
                ret.Add(new ProjectInfo(repoProj, repoUser, project));
            }

            return ret;
        }

        /// <inheritdoc/>
        public ProjectInfo GetByID(int id)
        {
            ProjectInfo ret = null;

            var repoProj = this.dataAccess.GetProjectsRepository();
            var repoUser = this.dataAccess.GetUsersRepository();

            var projEnt = repoProj.GetById(id);
            if (projEnt != null)
            {
                ret = new ProjectInfo(repoProj, repoUser, projEnt);
            }
            else
            {
                throw new ProjectManagementException(string.Format("Project #{0} doesn't exist", id));
            }

            return ret;
        }

        /// <inheritdoc/>
        public bool DeleteByID(int id)
        {
            bool ret = false;
            var projEnt = this.dataAccess.GetProjectsRepository().GetById(id);
            if (projEnt != null)
            {
                ret = this.dataAccess.GetProjectsRepository().Delete(projEnt) == 1;
            }
            else
            {
                throw new ProjectManagementException(string.Format("Project #{0} doesn't exist", id));
            }

            return ret;
        }

        /// <inheritdoc/>
        public void Rename(ProjectInfo prj, string newName)
        {
            if (prj != null && newName != null)
            {
                var projEnt = this.dataAccess.GetProjectsRepository().GetById(prj.ID);
                if (projEnt != null)
                {
                    projEnt.Name = newName;
                    this.dataAccess.FlushChanges();
                }
                else
                {
                    throw new ProjectManagementException(string.Format("Project #{0} doesn't exist", prj.ID));
                }
            }
            else
            {
                throw new ProjectManagementException("Invalid arguments", prj);
            }
        }

        /// <inheritdoc/>
        public void SetHomepage(ProjectInfo prj, string homepage)
        {
            if (prj != null)
            {
                var projEnt = this.dataAccess.GetProjectsRepository().GetById(prj.ID);
                if (projEnt != null)
                {
                    projEnt.Homepage = homepage;
                    this.dataAccess.FlushChanges();
                }
                else
                {
                    throw new ProjectManagementException(string.Format("Project #{0} doesn't exist", prj.ID));
                }
            }
            else
            {
                throw new ProjectManagementException("Invalid arguments", prj);
            }
        }

        /// <inheritdoc/>
        public void SetOwner(ProjectInfo prj, UserInfo usr)
        {
            if (prj != null)
            {
                var projEnt = this.dataAccess.GetProjectsRepository().GetById(prj.ID);
                var userEnt = usr != null ? this.dataAccess.GetUsersRepository().GetById(usr.ID) : null;
                if (projEnt != null)
                {
                    if (userEnt != null)
                    {
                        // Set owner
                        projEnt.Owner = usr.ID;
                    }
                    else
                    {
                        // Clear owner
                        projEnt.Owner = null;
                    }

                    this.dataAccess.FlushChanges();
                }
                else
                {
                    throw new ProjectManagementException(string.Format("Project #{0} doesn't exist", prj.ID));
                }
            }
            else
            {
                throw new ProjectManagementException("Invalid arguments", prj);
            }
        }

        /// <inheritdoc/>
        public ProjectInfo CreateProject(int id, string name, string homepage, int? ownerID, int? parentID, DateTime creationDate)
        {
            ProjectInfo ret = null;
            Project ent;
            int res;

            var repoProj = this.dataAccess.GetProjectsRepository();
            var repoUser = this.dataAccess.GetUsersRepository();

            if (name != null)
            {
                ent = new Project();
                ent.ID = id;
                ent.Name = name;
                ent.Homepage = homepage;
                ent.Parent = parentID;
                ent.Owner = ownerID;
                ent.CreationDate = creationDate;

                // Check if user #ownerID really exists.
                if (ownerID.HasValue)
                {
                    var user = repoUser.GetById(ownerID.Value);
                    if (user == null)
                    {
                        throw new ProjectManagementException(
                            string.Format("Cannot create a new project owned by user #{0} because they don't exist!", ownerID.Value));
                    }
                }

                res = repoProj.Create(ent);

                if (res == 1)
                {
                    ret = new ProjectInfo(repoProj, repoUser, ent);
                }
            }
            else
            {
                throw new ProjectManagementException("Cannot create a project with no name!");
            }

            return ret;
        }
    }
}

﻿// <copyright file="IssueManager.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker.Impl
{
    using System.Collections.Generic;
    using System.Linq;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.Database.Data;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Issue manager implementation.
    /// </summary>
    public class IssueManager : IIssueManagement
    {
        private IDataAccess dataAccess;

        /// <summary>
        /// Initializes a new instance of the <see cref="IssueManager"/> class.
        /// </summary>
        /// <param name="dataAccess">Data access interface.</param>
        public IssueManager(IDataAccess dataAccess)
        {
            this.dataAccess = dataAccess;
        }

        /// <inheritdoc/>
        public IssueInfo AssignIssue(int issueID, int userID)
        {
            IssueInfo ret = null;
            var repoUser = this.dataAccess.GetUsersRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoProj = this.dataAccess.GetProjectsRepository();

            var entUser = repoUser.GetById(userID);
            var entIssue = repoIssue.GetById(issueID);

            if (entUser == null)
            {
                throw new UserManagementException(string.Format("User #{0} does not exist!", userID));
            }

            if (entIssue == null)
            {
                throw new IssueManagementException(string.Format("Issue #{0} does not exist!", issueID));
            }

            entIssue.AssignedTo = entUser.ID;
            this.dataAccess.FlushChanges();
            ret = new IssueInfo(repoProj, repoUser, entIssue);

            return ret;
        }

        /// <inheritdoc/>
        public IssueInfo CloseIssue(int issueID)
        {
            IssueInfo ret = null;
            var repoUser = this.dataAccess.GetUsersRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoProj = this.dataAccess.GetProjectsRepository();

            var entIssue = repoIssue.GetById(issueID);

            if (entIssue == null)
            {
                throw new IssueManagementException(string.Format("Issue #{0} does not exist!", issueID));
            }

            entIssue.Solved = true;
            this.dataAccess.FlushChanges();
            ret = new IssueInfo(repoProj, repoUser, entIssue);

            return ret;
        }

        /// <inheritdoc/>
        public IssueInfo CreateNewIssue(int projectID, string title, int? assignedTo)
        {
            IssueInfo ret = null;
            var repoUser = this.dataAccess.GetUsersRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoProj = this.dataAccess.GetProjectsRepository();

            User user = null;
            if (assignedTo.HasValue)
            {
                user = repoUser.GetById(assignedTo.Value);
                if (user == null)
                {
                    throw new UserManagementException(string.Format("User #{0} does not exist!", assignedTo.Value));
                }
            }

            var project = repoProj.GetById(projectID);
            if (project == null)
            {
                throw new ProjectManagementException(string.Format("Project #{0} does not exist!", projectID));
            }

            var issue = new Issue();
            issue.Project = projectID;
            issue.ProjectRef = project;
            issue.Title = title;
            issue.AssignedTo = assignedTo;
            issue.AssignedToRef = user;
            issue.Solved = false;

            var res = repoIssue.Create(issue);

            if (res == 1)
            {
                ret = new IssueInfo(repoProj, repoUser, issue);
            }
            else
            {
                throw new IssueManagementException("Failed to create this issue!");
            }

            return ret;
        }

        /// <inheritdoc/>
        public void DeleteIssue(int issueID)
        {
            var repoUser = this.dataAccess.GetUsersRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoProj = this.dataAccess.GetProjectsRepository();

            var entIssue = repoIssue.GetById(issueID);

            if (entIssue == null)
            {
                throw new IssueManagementException(string.Format("Issue #{0} does not exist!", issueID));
            }

            var res = repoIssue.Delete(entIssue);
            if (res != 1)
            {
                throw new IssueManagementException(string.Format("Failed to delete issue #{0}!", issueID));
            }

            this.dataAccess.FlushChanges();
        }

        /// <inheritdoc/>
        public IQueryable<IssueInfo> GetAllIssues()
        {
            var ret = new List<IssueInfo>();
            var repoProj = this.dataAccess.GetProjectsRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoUser = this.dataAccess.GetUsersRepository();
            var issues = repoIssue.ReadAll();
            foreach (var issue in issues)
            {
                ret.Add(new IssueInfo(repoProj, repoUser, issue));
            }

            return ret.AsQueryable();
        }

        /// <inheritdoc/>
        public IQueryable<IssueInfo> GetAllIssues(int projectID)
        {
            var ret = new List<IssueInfo>();
            var repoProj = this.dataAccess.GetProjectsRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoUser = this.dataAccess.GetUsersRepository();
            var issues = repoIssue.ReadAll().Where(i => i.Project == projectID).AsEnumerable();
            foreach (var issue in issues)
            {
                ret.Add(new IssueInfo(repoProj, repoUser, issue));
            }

            return ret.AsQueryable();
        }

        /// <inheritdoc/>
        public IssueInfo GetById(int issueID)
        {
            IssueInfo ret = null;
            var repoProj = this.dataAccess.GetProjectsRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoUser = this.dataAccess.GetUsersRepository();
            var res = repoIssue.ReadAll().Where(i => i.ID == issueID);

            if (res.Count() > 0)
            {
                ret = new IssueInfo(repoProj, repoUser, res.First());
            }
            else
            {
                throw new IssueManagementException($"Issue #{issueID} doesn't exist");
            }

            return ret;
        }

        /// <inheritdoc/>
        public IEnumerable<IssueInfo> SearchForIssues(string s, int? projectID)
        {
            var repoProj = this.dataAccess.GetProjectsRepository();
            var repoIssue = this.dataAccess.GetIssuesRepository();
            var repoUser = this.dataAccess.GetUsersRepository();
            var q = repoIssue.ReadAll();
            if (projectID.HasValue)
            {
                q = q.Where(i => i.Project == projectID.Value);
            }

            foreach (var issue in q.Where(i => i.Title.ToUpper().Contains(s.ToUpper())))
            {
                yield return new IssueInfo(repoProj, repoUser, issue);
            }
        }
    }
}

﻿// <copyright file="IssueTracker.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.IssueTracker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.IssueTracker.Impl;

    /// <summary>
    /// Get your free interfaces.
    /// </summary>
    public class IssueTracker : IIssueTracker
    {
        private IDataAccess dataAccess = null;
        private IUserManagement users = null;
        private IProjectManagement projects = null;
        private IIssueManagement issues = null;
        private IStatistics statistics = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="IssueTracker"/> class.
        /// </summary>
        /// <param name="dataAccess">Data access interface.</param>
        public IssueTracker(IDataAccess dataAccess)
        {
            this.dataAccess = dataAccess;
            this.users = new UserManager(dataAccess);
            this.projects = new ProjectManager(dataAccess);
            this.issues = new IssueManager(dataAccess);
            this.statistics = new Statistics(dataAccess);
        }

        /// <inheritdoc/>
        public IUserManagement GetUserManagement()
        {
            return this.users;
        }

        /// <inheritdoc/>
        public IProjectManagement GetProjectManagement()
        {
            return this.projects;
        }

        /// <inheritdoc/>
        public IIssueManagement GetIssueManagement()
        {
            return this.issues;
        }

        /// <inheritdoc/>
        public IStatistics GetStatistics()
        {
            return this.statistics;
        }

        /// <inheritdoc/>
        public IWebProg3 ConnectToBackend(string baseURL)
        {
            return new WebProg3Impl(baseURL);
        }
    }
}

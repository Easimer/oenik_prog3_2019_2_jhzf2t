var searchData=
[
  ['dataaccessimpl_196',['DataAccessImpl',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_access_impl.html#a85dd6785bc1b85ff52b86fff4fb4280f',1,'Net::Easimer::Prog3::Database::DataAccessImpl']]],
  ['delete_197',['Delete',['../interface_net_1_1_easimer_1_1_prog3_1_1_database_1_1_i_repository.html#afd635e59fcb6471f4e50f051bbc7eb82',1,'Net::Easimer::Prog3::Database::IRepository']]],
  ['deletebyid_198',['DeleteByID',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_project_management.html#ac780ce7208a7f111035117418e8ef74e',1,'Net::Easimer::Prog3::IssueTracker::IProjectManagement']]],
  ['deleteissue_199',['DeleteIssue',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_issue_management.html#a3b680d2fc59cab56b57a92db4037e8ac',1,'Net.Easimer.Prog3.IssueTracker.IIssueManagement.DeleteIssue()'],['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_impl_1_1_issue_manager.html#a24877cc2ab982fddd5eb346f5c259b49',1,'Net.Easimer.Prog3.IssueTracker.Impl.IssueManager.DeleteIssue()']]],
  ['deleteuser_200',['DeleteUser',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_user_management.html#ab1a78d40c9cdb4b194e9bdb648177717',1,'Net::Easimer::Prog3::IssueTracker::IUserManagement']]],
  ['dispose_201',['Dispose',['../class_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_impl_1_1_web_prog3_impl.html#a35cfc4be508cda89eac233322349f5d0',1,'Net::Easimer::Prog3::IssueTracker::Impl::WebProg3Impl']]]
];

var searchData=
[
  ['readall_233',['ReadAll',['../interface_net_1_1_easimer_1_1_prog3_1_1_database_1_1_i_repository.html#a9332531b1bce316a0cd3fe0b638f6451',1,'Net::Easimer::Prog3::Database::IRepository']]],
  ['registernewuser_234',['RegisterNewUser',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_user_management.html#a19c2ebebfddd98f9de62ddd34ff93a77',1,'Net::Easimer::Prog3::IssueTracker::IUserManagement']]],
  ['rename_235',['Rename',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_project_management.html#a92445844ce3e85c0fe7bff7e04024df0',1,'Net::Easimer::Prog3::IssueTracker::IProjectManagement']]],
  ['renameuser_236',['RenameUser',['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_user_management.html#a7d541afa3f1a21e7445cd9885a659c5c',1,'Net.Easimer.Prog3.IssueTracker.IUserManagement.RenameUser(UserInfo subject, string firstName, string lastName)'],['../interface_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_i_user_management.html#af207cbbc33ddc3c7e8793b784a3f35cd',1,'Net.Easimer.Prog3.IssueTracker.IUserManagement.RenameUser(int userID, string firstName, string lastName)']]],
  ['repositoryerrorexception_237',['RepositoryErrorException',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_repository_error_exception.html#a3278e5b8a9e5766cfcb39fa9415b688f',1,'Net::Easimer::Prog3::Database::RepositoryErrorException']]]
];

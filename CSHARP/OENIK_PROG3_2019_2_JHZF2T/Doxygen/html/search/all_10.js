var searchData=
[
  ['telephone_117',['Telephone',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_user.html#a7b3254d158d982b971e12af7b2f0eabe',1,'Net::Easimer::Prog3::Database::Data::User']]],
  ['title_118',['Title',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_issue.html#aa7e561c998dd94edadf0fed3b59677cf',1,'Net::Easimer::Prog3::Database::Data::Issue']]],
  ['tostring_119',['ToString',['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_project.html#a24e7055941dbae15c37d3e013a81dd5f',1,'Net.Easimer.Prog3.Database.Data.Project.ToString()'],['../class_net_1_1_easimer_1_1_prog3_1_1_database_1_1_data_1_1_user.html#a11a3768c7c46cc61c97df2c375a0226d',1,'Net.Easimer.Prog3.Database.Data.User.ToString()']]],
  ['totalissues_120',['TotalIssues',['../struct_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_project_statistics.html#a0d38d049c1a1c6ca24cb43bfc7e2ef3a',1,'Net.Easimer.Prog3.IssueTracker.ProjectStatistics.TotalIssues()'],['../struct_net_1_1_easimer_1_1_prog3_1_1_issue_tracker_1_1_user_statistics.html#ad1aca10d29672ba1ff62f0aece445c59',1,'Net.Easimer.Prog3.IssueTracker.UserStatistics.TotalIssues()']]]
];

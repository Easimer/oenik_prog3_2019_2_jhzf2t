﻿// <copyright file="Client.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// User client logic.
    /// </summary>
    internal partial class Client
    {
        private IIssueTracker issueTracker;
        private Screen currentScreen;

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        /// <param name="it">Issue tracker instance.</param>
        public Client(IIssueTracker it)
        {
            this.issueTracker = it;
            this.currentScreen = Screen.Login;
        }

        private enum Screen
        {
            Invalid,
            Login,
            MainMenu,
            UserManagement,
            ProjectManagement,
            IssueManagement,
            StatisticsIssuesProjects,
            StatisticsIssuesUsers,
            Complaints,
        }

        /// <summary>
        /// Main loop.
        /// </summary>
        public void Loop()
        {
            IssueTrackerSession session = null;
            bool quit = false;
            while (!quit)
            {
                switch (this.currentScreen)
                {
                    case Screen.Login:
                        session = this.LoginScreen();
                        break;
                    case Screen.MainMenu:
                        this.MainMenuScreen(session);
                        break;
                    case Screen.UserManagement:
                        this.UserManagement(session);
                        break;
                    case Screen.ProjectManagement:
                        this.ProjectManagement(session);
                        break;
                    case Screen.IssueManagement:
                        this.IssueManagement(session);
                        break;
                    case Screen.StatisticsIssuesProjects:
                        this.StatisticsIssuesProjects(session);
                        break;
                    case Screen.StatisticsIssuesUsers:
                        this.StatisticsIssuesUsers(session);
                        break;
                    case Screen.Complaints:
                        this.ListComplaints(session);
                        break;
                    default:
                        quit = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Login screen.
        /// </summary>
        /// <returns>A session handle or null.</returns>
        private IssueTrackerSession LoginScreen()
        {
            IssueTrackerSession ret = null;
            AuthenticationException error = null;

            var baseURL = ConfigurationManager.AppSettings["webapi_base_url"];
            IWebProg3 auth = this.issueTracker.ConnectToBackend(baseURL);

            bool done = false;
            string username = string.Empty;
            string password = string.Empty;
            while (!done)
            {
                ImTUI.Begin();
                ImTUI.Input("Username", ref username);
                ImTUI.Password("Password", ref password);
                if (ImTUI.Button("Login"))
                {
                    try
                    {
                        int userID = auth.Authenticate(username, password);
                        ret = new IssueTrackerSession(userID);
                        done = true;
                        this.currentScreen = Screen.MainMenu;
                    }
                    catch (AuthenticationException ex)
                    {
                        error = ex;
                        done = false;
                    }
                }

                if (ImTUI.Button("Quit"))
                {
                    done = true;
                    this.currentScreen = Screen.Invalid;
                }

                if (error != null)
                {
                    ImTUI.Text("\nError: {0}", error.Message);
                }

                ImTUI.End(!done);
            }

            return ret;
        }

        /// <summary>
        /// Main menu screen implementation.
        /// </summary>
        /// <param name="session">Session.</param>
        private void MainMenuScreen(IssueTrackerSession session)
        {
            bool done = false;

            ImTUI.Unfocus();
            while (!done)
            {
                ImTUI.Begin();
                ImTUI.Text("Select an option:");
                if (ImTUI.Button("List of projects"))
                {
                    this.SwitchScreen(Screen.ProjectManagement);
                    done = true;
                }
                else if (ImTUI.Button("List of users"))
                {
                    this.SwitchScreen(Screen.UserManagement);
                    done = true;
                }
                else if (ImTUI.Button("List of all issues"))
                {
                    this.SwitchScreen(Screen.IssueManagement);
                    done = true;
                }
                else if (ImTUI.Button("Statistics: issues per project"))
                {
                    this.SwitchScreen(Screen.StatisticsIssuesProjects);
                    done = true;
                }
                else if (ImTUI.Button("Statistics: issues per user"))
                {
                    this.SwitchScreen(Screen.StatisticsIssuesUsers);
                    done = true;
                }
                else if (ImTUI.Button("Customer complaints"))
                {
                    this.SwitchScreen(Screen.Complaints);
                    done = true;
                }
                else if (ImTUI.Button("Exit"))
                {
                    this.SwitchScreen(Screen.Login);
                    done = true;
                }

                ImTUI.End(!done);
            }
        }

        /// <summary>
        /// Switch current screen.
        /// </summary>
        /// <param name="scr">New screen.</param>
        private void SwitchScreen(Screen scr)
        {
            this.currentScreen = scr;
            ImTUI.ResetScreen();
        }
    }
}

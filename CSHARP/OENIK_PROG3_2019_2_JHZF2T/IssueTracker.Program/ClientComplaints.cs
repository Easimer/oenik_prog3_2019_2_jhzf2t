﻿// <copyright file="ClientComplaints.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System.Configuration;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Implements the screens related to customer complaints.
    /// </summary>
    internal partial class Client
    {
        private void CreateIssueFromComplaint(Complaint c)
        {
            var issueMgmt = this.issueTracker.GetIssueManagement();
            IssueInfo issue = new IssueInfo();
            bool done = false;
            bool ok = false;
            var projectSelection = new ProjectSelectionWidget(this.issueTracker);
            int projectID = -1;

            ImTUI.ResetScreen();

            while (!done)
            {
                ImTUI.Begin();
                if (!projectSelection.Update())
                {
                    ImTUI.Text("No projects are available!");
                }

                switch (ImTUI.MenuBar("---", "Select", "---", "---", "---", "---", "---", "---", "---", "Cancel"))
                {
                    case 10:
                        done = true;
                        break;
                    case 2:
                        if (projectSelection.SelectedEntity != null)
                        {
                            ok = true;
                            projectID = (projectSelection.SelectedEntity as ProjectInfo).ID;
                        }

                        done = ok = true;
                        break;
                    default:
                        break;
                }

                ImTUI.End(!done);
            }

            ImTUI.ResetScreen();

            if (ok)
            {
                done = ok = false;

                while (!done)
                {
                    ImTUI.Begin();

                    // Display complaint
                    ImTUI.Text("========================");
                    ImTUI.Text($"From: {c.Author}\n");
                    ImTUI.Text(c.Contents);
                    ImTUI.Text("========================");
                    EntityEditor.Edit(issue, this.issueTracker);
                    if (ImTUI.Button("OK"))
                    {
                        ok = true;
                        done = true;
                    }
                    else if (ImTUI.Button("Cancel"))
                    {
                        done = true;
                    }

                    ImTUI.End(!done);
                }
            }

            ImTUI.ResetScreen();

            // Display results to user
            ImTUI.Begin();
            if (ok)
            {
                try
                {
                    issue = issueMgmt.CreateNewIssue(
                        projectID,
                        issue.Title,
                        issue.AssignedToRef != null ? (int?)issue.AssignedToRef.ID : null);
                    ImTUI.Text("Creation of the new issue was successful!");
                }
                catch (IssueManagementException ex)
                {
                    ImTUI.Text($"Couldn't create new issue: {ex.Message}");
                }
            }
            else
            {
                ImTUI.Text("Issue creation was cancelled!");
            }

            ImTUI.Text("Press any key to continue.");
            ImTUI.End(true);

            ImTUI.ResetScreen();
        }

        private void ListComplaints(IssueTrackerSession session)
        {
            bool done = false;
            var baseURL = ConfigurationManager.AppSettings["webapi_base_url"];
            var webapi = this.issueTracker.ConnectToBackend(baseURL);

            ImTUI.ResetScreen();
            ImTUI.Begin();
            ImTUI.Text("Retrieving complaints...");
            ImTUI.End();

            var complaints = webapi.GetComplaints();
            var complaintsEnum = complaints.GetEnumerator();
            bool exhausted = !complaintsEnum.MoveNext();

            while (!done)
            {
                var complaint = complaintsEnum.Current;
                ImTUI.Begin();

                if (exhausted)
                {
                    ImTUI.Text("No more complaints!\nPress F5 to go back to the beginning or F10 to exit to the main menu.");
                }
                else
                {
                    ImTUI.Text($"From: {complaint.Author}\n");
                    ImTUI.Text(complaint.Contents);
                }

                switch (ImTUI.MenuBar("---", "Next", "---", "---", "Refresh", "NewIssue", "---", "---", "---", "Back"))
                {
                    case 2:
                        exhausted = !complaintsEnum.MoveNext();
                        ImTUI.ForceClear();
                        break;
                    case 5:
                        complaints = webapi.GetComplaints();
                        complaintsEnum = complaints.GetEnumerator();
                        exhausted = !complaintsEnum.MoveNext();
                        break;
                    case 6:
                        this.CreateIssueFromComplaint(complaint);
                        break;
                    case 10:
                        done = true;
                        break;
                    default:
                        break;
                }

                ImTUI.End(!done);
            }

            this.SwitchScreen(Screen.MainMenu);
        }
    }
}

﻿// <copyright file="ImTUI.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Threading;

    /// <summary>
    /// Immediate Mode Terminal User Interface.
    /// </summary>
    public static partial class ImTUI
    {
        private static int focusedId = 0;
        private static int newFocusedId = 0;
        private static int nextId = 0;
        private static bool firstDraw = true;
        private static bool forceClear = false;
        private static int fnKeyPressed = 0;
        private static bool debugVisualize = false;

        private static Backbuffer buffer = null;

        /// <summary>
        /// Message box types.
        /// </summary>
        public enum MessageBoxType
        {
            /// <summary>
            /// A message box with a single OK button.
            /// </summary>
            OK,

            /// <summary>
            /// A message box with OK and Cancel buttons.
            /// </summary>
            OKCancel,

            /// <summary>
            /// A message box with Yes/No buttons.
            /// </summary>
            YesNo,
        }

        /// <summary>
        /// Message box buttons.
        /// </summary>
        public enum MessageBoxButtons
        {
            /// <summary>
            /// No button was pressed.
            /// </summary>
            None = -1,

            /// <summary>
            /// OK button was pressed.
            /// </summary>
            Ok = 0,

            /// <summary>
            /// Cancel button was pressed.
            /// </summary>
            Cancel = 1,

            /// <summary>
            /// Yes button was pressed.
            /// </summary>
            Yes = 0,

            /// <summary>
            /// No button was pressed.
            /// </summary>
            No = 1,
        }

        [Flags]
        private enum KeyFilter
        {
            FilterAll = 0,
            Displayables = 1,
            FunctionKeys = 2,
            Tab = 4,
            AllowAll = -1,
        }

        /// <summary>
        /// Begin an ImTUI 'frame'. Must be called before anything
        /// else is called.
        /// </summary>
        public static void Begin()
        {
            nextId = 0;
            if (buffer == null)
            {
                buffer = new Backbuffer(Console.WindowWidth, Console.WindowHeight);
            }

            buffer.Begin();

            if (forceClear)
            {
                buffer.Clear();
                forceClear = false;
            }

            buffer.Reposition();
            Console.CursorVisible = false;
        }

        /// <summary>
        /// Print formatted text.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <param name="a">Arguments.</param>
        public static void Text(string format, params object[] a)
        {
            buffer.WriteLine(format, a);
        }

        /// <summary>
        /// Read keyboard input and append it to the buffer passed in.
        /// </summary>
        /// <param name="label">Label to show before the input field.</param>
        /// <param name="buf">Input buffer string.</param>
        public static void Input(string label, ref string buf)
        {
            BaseInput(label, ref buf, DisplayTransformationIdentity);
        }

        /// <summary>
        /// Read keyboard input and append it to the buffer passed in.
        /// </summary>
        /// <param name="label">Label to show before the input field.</param>
        /// <param name="value">Input buffer.</param>
        public static void Input(string label, ref int value)
        {
            int old_value = value;
            int new_value;
            string value_str = value.ToString();
            BaseInput(label, ref value_str, DisplayTransformationIdentity);
            if (int.TryParse(value_str, out new_value))
            {
                value = new_value;
            }
        }

        /// <summary>
        /// Read keyboard input and append it to the buffer passed in. Contrary
        /// to calling Input(), this will not display the buffer's content but
        /// will show asterisks instead of letters and numbers.
        /// </summary>
        /// <param name="label">Label to show before the input field.</param>
        /// <param name="buf">Input buffer string.</param>
        public static void Password(string label, ref string buf)
        {
            BaseInput(
                label,
                ref buf,
                (l, b, f) => DisplayTransformation(l, b, f, x => '*'));
        }

        /// <summary>
        /// Button widget. Returns true when pressed.
        /// </summary>
        /// <param name="label">Button label.</param>
        /// <returns>True when pressed, false otherwise.</returns>
        public static bool Button(string label)
        {
            bool ret = false;
            int id = GetNextId();
            bool focused = IsFocusedOn(id);

            if (focused)
            {
                ConsoleKeyInfo kvinf;
                while (buffer.KeyAvailable)
                {
                    kvinf = buffer.ReadKey();
                    if (HandleKeyInput(kvinf))
                    {
                        if (kvinf.Key == ConsoleKey.Enter)
                        {
                            ret = true;
                        }
                    }

                    focused = IsFocusedOn(id);
                }
            }

            if (focused)
            {
                buffer.WriteLine("[ {0} ]", label);
            }
            else
            {
                buffer.WriteLine(label);
            }

            return ret;
        }

        /// <summary>
        /// Checkbox widget.
        /// Returns the checkbox state.
        /// </summary>
        /// <param name="label">Checkbox label.</param>
        /// <param name="value">Ref to a bool where the state is stored.</param>
        /// <returns>Checkbox state.</returns>
        public static bool Checkbox(string label, ref bool value)
        {
            bool ret = false;
            int id = GetNextId();
            bool focused = IsFocusedOn(id);

            if (focused)
            {
                ConsoleKeyInfo kvinf;
                while (buffer.KeyAvailable)
                {
                    kvinf = buffer.ReadKey();
                    if (HandleKeyInput(kvinf))
                    {
                        if (kvinf.Key == ConsoleKey.Enter || kvinf.Key == ConsoleKey.Spacebar)
                        {
                            value = !value;
                        }
                    }
                }
            }

            if (focused)
            {
                buffer.WriteLine("({0}) [ {1} ]", value ? 'X' : ' ', label);
            }
            else
            {
                buffer.WriteLine("({0}) {1}", value ? 'X' : ' ', label);
            }

            ret = value;

            return ret;
        }

        /// <summary>
        /// Ends an ImTUI 'frame'. Must be called when
        /// everything's done or nothing will be drawn.
        /// </summary>
        /// <param name="waitForInput">Should <see cref="BlockUntilUserInput">BlockUntilUserInput</see> be called.</param>
        public static void End(bool waitForInput = false)
        {
            if (newFocusedId < 0)
            {
                newFocusedId = 0;
            }

            if (newFocusedId >= nextId)
            {
                newFocusedId = nextId - 1;
            }

            if (buffer.ShouldBeRedrawn())
            {
                buffer.Redraw(debugVisualize);
            }

            focusedId = newFocusedId;

            if (waitForInput)
            {
                BlockUntilUserInput();
            }

            buffer.End();
        }

        /// <summary>
        /// Blocks the executing thread until any keyboard input.
        /// </summary>
        public static void BlockUntilUserInput()
        {
            if (!firstDraw)
            {
                buffer.PeekKey(true);
                firstDraw = true;
            }
            else
            {
                firstDraw = false;
            }
        }

        /// <summary>
        /// Unfocus focused widget.
        /// </summary>
        public static void Unfocus()
        {
            focusedId = 0;
            newFocusedId = 0;
        }

        /// <summary>
        /// Checks if a function key press has happend and returns the pressed
        /// function key's number or zero if no function key was pressed.
        /// </summary>
        /// <returns>Function key number or zero on error.</returns>
        public static int WasFunctionKeyPressed()
        {
            int ret = fnKeyPressed;
            fnKeyPressed = 0;
            return ret;
        }

        /// <summary>
        /// Shows a message box with one or more buttons to press.
        /// </summary>
        /// <param name="type">Message box type.</param>
        /// <param name="buttonClicked">Which button was pressed.</param>
        /// <param name="message">Formatting string to display.</param>
        /// <param name="a">Format arguments.</param>
        /// <returns>Whether any buttons were pressed or not.</returns>
        public static bool MessageBox(MessageBoxType type, out MessageBoxButtons buttonClicked, string message, params object[] a)
        {
            bool ret = false;
            buttonClicked = MessageBoxButtons.None;
            buffer.ForceRedraw(); // Clear background

            Text(message, a);
            switch (type)
            {
                case MessageBoxType.OK:
                    buttonClicked = Button("OK") ? MessageBoxButtons.Ok : buttonClicked;
                    break;
                case MessageBoxType.OKCancel:
                    buttonClicked = Button("OK") ? MessageBoxButtons.Ok : buttonClicked;
                    buttonClicked = Button("Cancel") ? MessageBoxButtons.Cancel : buttonClicked;
                    break;
                case MessageBoxType.YesNo:
                    buttonClicked = Button("Yes") ? MessageBoxButtons.Yes : buttonClicked;
                    buttonClicked = Button("No") ? MessageBoxButtons.No : buttonClicked;
                    break;
            }

            ret = buttonClicked != MessageBoxButtons.None;

            if (ret)
            {
                buffer.Clear();
                buffer.ForceRedraw();
            }

            return ret;
        }

        /// <summary>
        /// Shows a message box with one or more buttons to press.
        /// Use this overload when you want to don't want to know what button was pressed,
        /// i.e. a box showing a simple message with an OK button.
        /// </summary>
        /// <param name="type">Message box type.</param>
        /// <param name="message">Formatting string to display.</param>
        /// <param name="a">Format arguments.</param>
        /// <returns>Whether any buttons were pressed or not.</returns>
        public static bool MessageBox(MessageBoxType type, string message, params object[] a)
        {
            MessageBoxButtons sel;
            return MessageBox(type, out sel, message, a);
        }

        /// <summary>
        /// Shows a message box with an OK button.
        /// </summary>
        /// <param name="message">Formatting string to display.</param>
        /// <param name="a">Format arguments.</param>
        /// <returns>Whether any buttons were pressed or not.</returns>
        public static bool MessageBox(string message, params object[] a)
        {
            MessageBoxButtons sel;
            return MessageBox(MessageBoxType.OK, out sel, message, a);
        }

        /// <summary>
        /// Force a whole screen clear.
        /// </summary>
        public static void ForceClear()
        {
            forceClear = true;
        }

        /// <summary>
        /// Draw a menubar.
        /// </summary>
        /// <param name="items">Menu items.</param>
        /// <returns>Pressed function key or zero.</returns>
        public static int MenuBar(params string[] items)
        {
            int ret = 0;

            var oldX = buffer.CursorLeft;
            var oldY = buffer.CursorTop;

            buffer.CursorLeft = 0;
            buffer.CursorTop = buffer.WindowHeight - 1;
            for (int i = 0; i < items.Length; i++)
            {
                buffer.Write("| {0} {1} ", i + 1, items[i]);
            }

            buffer.CursorLeft = oldX;
            buffer.CursorTop = oldY;

            ret = WasFunctionKeyPressed();

            return ret;
        }

        /// <summary>
        /// Table column.
        /// </summary>
        /// <param name="s">Column data.</param>
        /// <param name="colWidth">Column width.</param>
        public static void TableColumn(string s, int colWidth = 16)
        {
            int nextX = buffer.CursorLeft + colWidth;
            buffer.Write(s);
            buffer.CursorLeft = nextX + 1;
        }

        /// <summary>
        /// Begin table row.
        /// </summary>
        /// <param name="header">Is this a header row.</param>
        /// <param name="selected">Is the row selected.</param>
        /// <returns>A value indicating whether this row was selected.</returns>
        public static bool TableBeginRow(bool header = false, bool selected = false)
        {
            bool ret = false;
            var id = GetNextId();
            var focused = IsFocusedOn(id);

            if (focused)
            {
                ConsoleKeyInfo kvinf;
                while (buffer.KeyAvailable)
                {
                    kvinf = buffer.ReadKey();
                    if (HandleKeyInput(kvinf))
                    {
                        if (kvinf.Key == ConsoleKey.Enter)
                        {
                            ret = !header;
                        }
                    }

                    focused = IsFocusedOn(id);
                }
            }

            if (header)
            {
                buffer.Write("+ ");
            }
            else
            {
                if (focused)
                {
                    buffer.Write("> ");
                }
                else
                {
                    if (selected)
                    {
                        buffer.Write("* ");
                    }
                    else
                    {
                        buffer.Write("  ");
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// End table row.
        /// </summary>
        public static void TableEndRow()
        {
            buffer.Write('\n');
        }

        /// <summary>
        /// Clears the whole screen and resets any state.
        /// </summary>
        public static void ResetScreen()
        {
            ImTUI.Unfocus();
            ImTUI.ForceClear();
        }

        /// <summary>
        /// Read keyboard input and append it to the buffer passed in.
        /// </summary>
        /// <param name="label">Label to show before the input field.</param>
        /// <param name="buf">Input buffer string.</param>
        /// <param name="displayFunction">Subroutine that will display the label and the string buffer.</param>
        private static void BaseInput(string label, ref string buf, Action<string, string, bool> displayFunction)
        {
            var id = GetNextId();
            bool focused = IsFocusedOn(id);

            if (focused)
            {
                bool done = false;
                ImTUI.ConsoleKeyInfo kvinf;

                // Eat as much input as we can until we get a non-displayable
                // input.
                while (!done && buffer.KeyAvailable)
                {
                    kvinf = buffer.ReadKey(KeyFilter.Displayables, false);
                    if (HandleKeyInput(kvinf))
                    {
                        if (kvinf.Present && kvinf.Key == ConsoleKey.Backspace)
                        {
                            if (kvinf.Modifiers.HasFlag(ConsoleModifiers.Control))
                            {
                                buf = string.Empty;
                            }
                            else if (buf.Length > 0)
                            {
                                buf = buf.Substring(0, buf.Length - 1);
                            }
                        }
                        else
                        {
                            if (kvinf.Key != ConsoleKey.Enter)
                            {
                                buf += kvinf.KeyChar;
                            }
                        }
                    }
                    else
                    {
                        done = true;
                    }

                    focused = IsFocusedOn(id);
                }
            }

            displayFunction.Invoke(label, buf, focused);
        }

        private static void DisplayTransformation(string label, string buf, bool focused, Func<char, char> transformation)
        {
            if (focused)
            {
                buffer.Write("[ {0} ]: ", label);
            }
            else
            {
                buffer.Write("{0}: ", label);
            }

            foreach (var ch in buf)
            {
                buffer.Write(transformation(ch));
            }

            if (focused)
            {
                buffer.Write("█");
            }

            buffer.Write('\n');
        }

        private static void DisplayTransformationIdentity(string label, string buf, bool focused)
        {
            DisplayTransformation(label, buf, focused, x => x);
        }

        /// <summary>
        /// WIDGET API: Determines whether the passed in ID is the one
        /// currently in focus.
        /// </summary>
        /// <param name="id">The ID in question.</param>
        /// <returns>Whether the passed in ID is the one currently in focus.</returns>
        private static bool IsFocusedOn(int id)
        {
            return focusedId == id;
        }

        /// <summary>
        /// WIDGET API: Generate a new frame-unique ID.
        /// </summary>
        /// <returns>A frame-unique ID.</returns>
        private static int GetNextId()
        {
            return nextId++;
        }

        /// <summary>
        /// INTERNAL API: Handle tab input.
        /// </summary>
        /// <param name="kv">Keyboard input.</param>
        private static void TabInput(ConsoleKeyInfo kv)
        {
            if (kv.Modifiers.HasFlag(ConsoleModifiers.Shift))
            {
                newFocusedId = focusedId - 1;
            }
            else
            {
                newFocusedId = focusedId + 1;
            }

            if (newFocusedId < 0)
            {
                newFocusedId = 0;
            }

            buffer.ForceRedraw();
        }

        /// <summary>
        /// WIDGET API: Returns true if the input is processable by the widget
        /// code.
        /// Ex.: Widgets cannot process TAB key inputs because
        /// that's used to switch between widgets.
        /// </summary>
        /// <param name="kv">Key input.</param>
        /// <returns>Should the caller handle this keyboard input event.</returns>
        private static bool HandleKeyInput(ConsoleKeyInfo kv)
        {
            bool ret = false;

            if (kv.Present)
            {
                switch (kv.Key)
                {
                    case ConsoleKey.F1: fnKeyPressed = 1; break;
                    case ConsoleKey.F2: fnKeyPressed = 2; break;
                    case ConsoleKey.F3: fnKeyPressed = 3; break;
                    case ConsoleKey.F4: fnKeyPressed = 4; break;
                    case ConsoleKey.F5: fnKeyPressed = 5; break;
                    case ConsoleKey.F6: fnKeyPressed = 6; break;
                    case ConsoleKey.F7: fnKeyPressed = 7; break;
                    case ConsoleKey.F8: fnKeyPressed = 8; break;
                    case ConsoleKey.F9: fnKeyPressed = 9; break;
                    case ConsoleKey.F10: fnKeyPressed = 10; break;
                    case ConsoleKey.F11: fnKeyPressed = 11; break;
                    //// case ConsoleKey.F12: fnKeyPressed = 12; break;
                    case ConsoleKey.Tab:
                        TabInput(kv);
                        break;
                    case ConsoleKey.F12:
#if DEBUG
                        debugVisualize = !debugVisualize;
#endif
                        break;
                    default:
                        ret = true;
                        break;
                }
            }

            return ret;
        }
    }
}

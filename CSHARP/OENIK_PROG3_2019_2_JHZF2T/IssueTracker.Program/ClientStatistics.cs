﻿// <copyright file="ClientStatistics.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System.Linq;

    /// <summary>
    /// Implements the statistical screens.
    /// </summary>
    internal partial class Client
    {
        private void StatisticsIssuesProjects(IssueTrackerSession session)
        {
            bool done = false;
            var stats = this.issueTracker.GetStatistics();

            ImTUI.ResetScreen();
            while (!done)
            {
                ImTUI.Begin();
                ImTUI.TableBeginRow(true);
                ImTUI.TableColumn("#", 4);
                ImTUI.TableColumn("Name", 32);
                ImTUI.TableColumn("All issues");
                ImTUI.TableColumn("Solved issues");
                ImTUI.TableColumn("Unsolved issues");
                ImTUI.TableEndRow();
                foreach (var project in stats.GetProjectStatistics())
                {
                    ImTUI.TableBeginRow();
                    ImTUI.TableColumn(project.ProjectID.ToString(), 4);
                    ImTUI.TableColumn(project.ProjectName, 32);
                    ImTUI.TableColumn(project.TotalIssues.ToString());
                    ImTUI.TableColumn(project.SolvedIssues.ToString());
                    ImTUI.TableColumn(project.UnsolvedIssues.ToString());
                    ImTUI.TableEndRow();
                }

                ImTUI.Text("\nPress F10 to continue...\n");
                ImTUI.End(!done);
                done = ImTUI.WasFunctionKeyPressed() == 10;
            }

            this.SwitchScreen(Screen.MainMenu);
        }

        private void StatisticsIssuesUsers(IssueTrackerSession session)
        {
            bool done = false;
            var stats = this.issueTracker.GetStatistics();

            ImTUI.ResetScreen();
            while (!done)
            {
                ImTUI.Begin();
                ImTUI.TableBeginRow(true);
                ImTUI.TableColumn("#", 4);
                ImTUI.TableColumn("Name", 32);
                ImTUI.TableColumn("All issues");
                ImTUI.TableColumn("Solved issues");
                ImTUI.TableColumn("Unsolved issues");
                ImTUI.TableEndRow();
                foreach (var project in stats.GetUserStatistics())
                {
                    ImTUI.TableBeginRow();
                    ImTUI.TableColumn(project.UserID.ToString(), 4);
                    ImTUI.TableColumn(project.UserName, 32);
                    ImTUI.TableColumn(project.TotalIssues.ToString());
                    ImTUI.TableColumn(project.SolvedIssues.ToString());
                    ImTUI.TableColumn(project.UnsolvedIssues.ToString());
                    ImTUI.TableEndRow();
                }

                ImTUI.Text("\nPress F10 to continue...\n");
                ImTUI.End(!done);
                done = ImTUI.WasFunctionKeyPressed() == 10;
            }

            this.SwitchScreen(Screen.MainMenu);
        }
    }
}

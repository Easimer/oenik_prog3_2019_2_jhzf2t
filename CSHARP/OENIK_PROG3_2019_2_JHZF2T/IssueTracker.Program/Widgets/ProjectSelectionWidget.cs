﻿// <copyright file="ProjectSelectionWidget.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Project selection widget.
    /// </summary>
    [Selector(typeof(ProjectInfo))]
    internal class ProjectSelectionWidget : BaseSelectionWidget
    {
        private IProjectManagement projMgmt;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectSelectionWidget"/> class.
        /// </summary>
        /// <param name="issueTracker">Project management interface.</param>
        public ProjectSelectionWidget(IIssueTracker issueTracker)
            : base(issueTracker)
        {
            this.projMgmt = issueTracker.GetProjectManagement();
        }

        /// <inheritdoc/>
        protected override IEnumerable<string> GetColumnNames()
        {
            return new string[]
            {
                "#",
                "Name",
                "Owner",
                "Homepage",
                "Created on",
                "Parent",
                "Language",
            };
        }

        /// <inheritdoc/>
        protected override int GetColumnWidth(int colIdx)
        {
            switch (colIdx)
            {
                case 0: return 4;
                case 1: return 24;
                case 4:
                    return 12;
                default:
                    return 16;
            }
        }

        /// <inheritdoc/>
        protected override IEnumerable<Tuple<object[], object>> GetTableData()
        {
            foreach (var row in this.projMgmt.GetAllProjects())
            {
                var ret = new Tuple<object[], object>(
                    new object[]
                    {
                        row.ID,
                        row.Name,
                        row.Owner.HasValue ? row.OwnerRef.Username : "(none)",
                        row.Homepage,
                        row.CreationDate.ToShortDateString(),
                        row.Parent.HasValue ? row.ParentRef.Name : "(none)",
                        row.Lang,
                    }, row);

                yield return ret;
            }
        }
    }
}

﻿// <copyright file="FilteredIssueSelectionWidget.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Filtered issue selection widget.
    /// </summary>
    internal class FilteredIssueSelectionWidget : IssueSelectionWidget
    {
        private string filter;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilteredIssueSelectionWidget"/> class.
        /// </summary>
        /// <param name="issueTracker">Issue tracker instance.</param>
        /// <param name="filter">Filter string.</param>
        public FilteredIssueSelectionWidget(IIssueTracker issueTracker, string filter)
            : base(issueTracker)
        {
            this.filter = filter;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FilteredIssueSelectionWidget"/> class.
        /// </summary>
        /// <param name="issueTracker">Issue tracker instance.</param>
        /// <param name="projectID">Project ID.</param>
        /// <param name="filter">Filter string.</param>
        public FilteredIssueSelectionWidget(IIssueTracker issueTracker, int projectID, string filter)
            : base(issueTracker, projectID)
        {
            this.filter = filter;
        }

        /// <inheritdoc/>
        protected override IEnumerable<Tuple<object[], object>> GetTableData()
        {
            foreach (var row in this.IssueMgmt.SearchForIssues(this.filter, this.ProjectID))
            {
                var ret = new Tuple<object[], object>(
                    new object[]
                    {
                        row.ID,
                        row.Title,
                        row.Solved ? "✓" : "x",
                        row.AssignedTo.HasValue ? row.AssignedToRef.Username : "(none)",
                    }, row);

                yield return ret;
            }
        }
    }
}

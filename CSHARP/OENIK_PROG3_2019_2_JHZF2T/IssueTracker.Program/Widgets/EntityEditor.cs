﻿// <copyright file="EntityEditor.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Reflection;
    using Net.Easimer.Prog3.Database;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Entity editor class.
    /// </summary>
    internal static class EntityEditor
    {
        /// <summary>
        /// Displays an entity editor widget.
        /// </summary>
        /// <typeparam name="T">Type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <param name="issueTracker">Issue tracker instance.</param>
        public static void Edit<T>(T entity, IIssueTracker issueTracker)
        {
            foreach (var field in typeof(T).GetProperties())
            {
                if (field.GetCustomAttribute(typeof(IgnoredByEditorAttribute)) == null)
                {
                    ImTUI.Text("-----------------");
                    switch (field.PropertyType.FullName)
                    {
                        case "System.String":
                            EditString(entity, field);
                            break;
                        case "System.Int32":
                            EditInt(entity, field);
                            break;
                        case "System.Boolean":
                            EditBoolean(entity, field);
                            break;
                        default:
                            EditGeneric(entity, field, issueTracker);
                            break;
                    }
                }
            }
        }

        private static IEditorWidget GetWidgetFor(object entity, PropertyInfo field, IIssueTracker issueTracker)
        {
            IEditorWidget ret = null;
            var value = field.GetValue(entity);
            if (value != null)
            {
                // NOTE(danielm): can't edit null values.
                ret = EditorAttribute.GetWidgetForType(value);
            }

            if (ret == null)
            {
                ret = SelectorAttribute.GetSelectionScreenForType(field.PropertyType, issueTracker);
            }

            return ret;
        }

        /// <summary>
        /// Gets the custom description of the field or if it has none then its name.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="appendType">Appends the type of the field if no custom description is available.</param>
        /// <returns>Custom description or field name.</returns>
        private static string GetNameOfField(PropertyInfo field, bool appendType = false)
        {
            string ret = field.Name;
            var descAttr = field.GetCustomAttribute<EditorDescriptionAttribute>();

            if (descAttr != null)
            {
                ret = descAttr.Description;
            }
            else
            {
                if (appendType)
                {
                    ret = string.Format("{0} ({1})", ret, field.PropertyType.Name);
                }
            }

            return ret;
        }

        private static void EditString(object entity, PropertyInfo field)
        {
            var requiredAttr = field.GetCustomAttribute<EditorRequiredAttribute>();
            var value = (string)field.GetValue(entity);
            bool required = requiredAttr != null;
            bool isValueNull = value == null;
            bool present = !isValueNull;
            string fieldName = GetNameOfField(field);

            if (required)
            {
                if (isValueNull)
                {
                    value = string.Empty;
                }

                ImTUI.Input(fieldName, ref value);
            }
            else
            {
                if (ImTUI.Checkbox($"{fieldName}", ref present))
                {
                    if (isValueNull)
                    {
                        value = string.Empty;
                    }

                    ImTUI.Input("Value", ref value);
                }
                else
                {
                    value = null;
                }
            }

            field.SetValue(entity, value);
        }

        private static void EditInt(object entity, PropertyInfo field)
        {
            var value = (int)field.GetValue(entity);
            string fieldName = GetNameOfField(field);
            ImTUI.Input(fieldName, ref value);
            field.SetValue(entity, value);
        }

        private static void EditBoolean(object entity, PropertyInfo field)
        {
            var value = (bool)field.GetValue(entity);
            string fieldName = GetNameOfField(field);
            ImTUI.Checkbox(fieldName, ref value);
            field.SetValue(entity, value);
        }

        private static void EditGeneric(object entity, PropertyInfo field, IIssueTracker issueTracker)
        {
            string buttonTag;
            var descAttr = field.GetCustomAttribute<EditorDescriptionAttribute>();
            string fieldName = GetNameOfField(field, true);

            // Entity provided custom description for this field
            if (descAttr != null)
            {
                buttonTag = descAttr.Description;
            }
            else
            {
                buttonTag = $"'{fieldName}' ({field.PropertyType.Name})";
            }

            if (ImTUI.Button(buttonTag))
            {
                var widget = GetWidgetFor(entity, field, issueTracker);
                if (widget != null)
                {
                    bool done = false;
                    ImTUI.ResetScreen();
                    while (!done)
                    {
                        ImTUI.Begin();
                        if (!widget.Update())
                        {
                            ImTUI.Text("The list is empty!");
                        }

                        ImTUI.Text("\n");
                        if (ImTUI.Button("OK"))
                        {
                            done = true;
                            field.SetValue(entity, widget.Edited);
                        }
                        else if (ImTUI.Button("Cancel"))
                        {
                            done = true;
                        }

                        ImTUI.End(!done);
                    }

                    ImTUI.ResetScreen();
                }
                else
                {
                    ImTUI.Begin();
                    ImTUI.MessageBox("No selection / editor screen was defined for this data entity! Ask a programmer.");
                    ImTUI.End(true);
                    ImTUI.ResetScreen();
                }
            }

            var fieldValue = field.GetValue(entity);
            if (fieldValue != null)
            {
                ImTUI.Text("\t{0}", fieldValue.ToString());
            }
        }
    }
}

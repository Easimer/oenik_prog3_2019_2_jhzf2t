﻿// <copyright file="BaseSelectionWidget.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Entity list widget.
    /// </summary>
    internal abstract class BaseSelectionWidget : IEditorWidget
    {
        private object selectedEntity;
        private object[] selectedColumns;
        private int selectedRowID;
        private Tuple<object[], object>[] tableCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseSelectionWidget"/> class.
        /// </summary>
        /// <param name="issueTracker">Issue tracker interface instance.</param>
        public BaseSelectionWidget(IIssueTracker issueTracker)
        {
            this.InvalidateCache();
        }

        /// <summary>
        /// Gets the selected row's index.
        /// </summary>
        public int SelectedRowIndex => this.selectedRowID;

        /// <summary>
        /// Gets the selected row.
        /// </summary>
        public object[] SelectedColumns => this.selectedColumns;

        /// <summary>
        /// Gets the selected entity.
        /// </summary>
        public object SelectedEntity => this.selectedEntity;

        /// <inheritdoc/>
        public object Edited { get => this.SelectedEntity; set => throw new InvalidOperationException(); }

        /// <summary>
        /// Gets the selected entity casted to T.
        /// </summary>
        /// <typeparam name="T">Entity type.</typeparam>
        /// <returns>Entity data.</returns>
        public T GetSelectedEntity<T>()
        {
            return (T)this.SelectedEntity;
        }

        /// <summary>
        /// Runs the widget's logic.
        /// </summary>
        /// <returns>Returns a value indicating whether any data was displayed.</returns>
        public bool Update()
        {
            bool ret = false;
            ImTUI.TableBeginRow(true);
            int i = 0, rowIdx = 0;
            foreach (var colName in this.GetColumnNames())
            {
                ImTUI.TableColumn(colName, this.GetColumnWidth(i));
                i++;
            }

            ImTUI.TableEndRow();

            // foreach (var row in this.GetTableData())
            foreach (var row in this.GetTableDataCache())
            {
                ret = true;
                if (ImTUI.TableBeginRow(false, this.SelectedRowIndex == rowIdx))
                {
                    this.selectedRowID = rowIdx;
                    this.selectedColumns = row.Item1;
                    this.selectedEntity = row.Item2;
                }

                i = 0;

                foreach (var col in row.Item1)
                {
                    string value = col != null ? col.ToString() : string.Empty;
                    ImTUI.TableColumn(value, this.GetColumnWidth(i));
                    i++;
                }

                ImTUI.TableEndRow();
                rowIdx++;
            }

            return ret;
        }

        /// <summary>
        /// Returns the rows of the table converted to a type.
        /// </summary>
        /// <typeparam name="T">Table row type.</typeparam>
        /// <returns>Table rows.</returns>
        public IEnumerable<Tuple<object[], T>> GetTableRows<T>()
        {
            foreach (var row in this.GetTableData())
            {
                yield return new Tuple<object[], T>(row.Item1, (T)row.Item2);
            }
        }

        /// <inheritdoc/>
        public void InvalidateCache()
        {
            this.selectedRowID = -1;
            this.selectedColumns = null;
            this.selectedEntity = null;
            this.tableCache = null;
        }

        /// <summary>
        /// Gets the column names.
        /// </summary>
        /// <returns>Returns an array containing the names of the columns.</returns>
        protected abstract IEnumerable<string> GetColumnNames();

        /// <summary>
        /// Gets a column's width.
        /// </summary>
        /// <param name="colIdx">Column index.</param>
        /// <returns>The column's width.</returns>
        protected abstract int GetColumnWidth(int colIdx);

        /// <summary>
        /// Transforms an entity into an object array.
        /// </summary>
        /// <returns>The table data.</returns>
        protected virtual IEnumerable<Tuple<object[], object>> GetTableData()
        {
            return new Tuple<object[], object>[0];
        }

        private Tuple<object[], object>[] GetTableDataCache()
        {
            if (this.tableCache == null)
            {
                this.tableCache = this.GetTableData().ToArray();
            }

            return this.tableCache;
        }
    }
}

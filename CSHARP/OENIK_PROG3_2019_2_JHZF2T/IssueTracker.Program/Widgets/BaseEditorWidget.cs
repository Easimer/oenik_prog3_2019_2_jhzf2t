﻿// <copyright file="BaseEditorWidget.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    /// <summary>
    /// Base editor widget.
    /// </summary>
    internal abstract class BaseEditorWidget : IEditorWidget
    {
        private object edited;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseEditorWidget"/> class.
        /// </summary>
        /// <param name="edited">Edited object.</param>
        public BaseEditorWidget(object edited)
        {
            this.edited = edited;
        }

        /// <summary>
        /// Gets or sets the edited object.
        /// </summary>
        public object Edited { get => this.edited; set => this.edited = value; }

        /// <inheritdoc/>
        public void InvalidateCache()
        {
        }

        /// <summary>
        /// Widget logic code.
        /// </summary>
        /// <returns>True.</returns>
        public abstract bool Update();
    }
}

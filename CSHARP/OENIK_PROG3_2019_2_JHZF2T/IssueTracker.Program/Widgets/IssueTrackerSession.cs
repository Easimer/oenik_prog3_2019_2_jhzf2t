﻿// <copyright file="IssueTrackerSession.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;

    /// <summary>
    /// Issue tracker session.
    /// </summary>
    public class IssueTrackerSession
    {
        /// <summary>
        /// Session User ID.
        /// </summary>
        private int userID;

        /// <summary>
        /// Initializes a new instance of the <see cref="IssueTrackerSession"/> class.
        /// </summary>
        /// <param name="userID">User ID.</param>
        public IssueTrackerSession(int userID)
        {
            this.userID = userID;
        }

        /// <summary>
        /// Gets or sets the user ID.
        /// </summary>
        public int UserID { get => this.userID; set => this.userID = value; }
    }
}

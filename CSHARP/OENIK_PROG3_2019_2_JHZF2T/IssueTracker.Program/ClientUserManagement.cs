﻿// <copyright file="ClientUserManagement.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// User management screen implementation.
    /// </summary>
    internal partial class Client
    {
        private void RenameUser(int userID)
        {
            bool done = false;
            bool ok = true;
            var userMgmt = this.issueTracker.GetUserManagement();
            UserInfo user = userMgmt.GetUserByID(userID);
            string userName = user.Username;
            string firstName = user.FirstName;
            string lastName = user.LastName;

            ImTUI.ForceClear();

            while (!done)
            {
                ImTUI.Begin();
                ImTUI.Text($"Renaming user #{userID} '${userName}'");
                ImTUI.Input("First name", ref firstName);
                ImTUI.Input("Last name", ref lastName);
                if (ImTUI.Button("OK"))
                {
                    done = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    done = true;
                    ok = false;
                }

                ImTUI.End(!done);
            }

            if (ok)
            {
                userMgmt.RenameUser(userID, firstName, lastName);
            }

            ImTUI.ForceClear();
        }

        private void ModifyUser(int userID)
        {
            var opt = new string[]
            {
                "Rename",
            };
            var act = new Action<int>[]
            {
                u => this.RenameUser(u),
            };
            this.ModifyEntityCommon(userID, opt, act, "user");
        }

        /// <summary>
        /// Subroutine implementing a user selection screen.
        /// </summary>
        /// <param name="issueTracker">Issue tracker instance.</param>
        /// <returns>UserInfo instance or null if none were selected.</returns>
        private UserInfo SelectAUser(IIssueTracker issueTracker)
        {
            bool done = false;
            UserInfo ret = null;
            ImTUI.ForceClear();
            var tableWidget = new UserSelectionWidget(issueTracker);

            while (!done)
            {
                ImTUI.Begin();

                if (!tableWidget.Update())
                {
                    ImTUI.Text("The list is empty!");
                }

                ImTUI.Text("\n");

                if (ImTUI.Button("OK"))
                {
                    done = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    done = true;
                }

                ImTUI.End();
            }

            ret = tableWidget.GetSelectedEntity<UserInfo>();

            ImTUI.ForceClear();

            return ret;
        }

        /// <summary>
        /// User creation screen.
        /// </summary>
        /// <param name="session">Session.</param>
        /// <param name="userMgmt">User management interface.</param>
        private void UserManagementCreate(IssueTrackerSession session, IUserManagement userMgmt)
        {
            bool done = false;
            bool ok = true;

            UserInfo uf = new UserInfo();

            ImTUI.ForceClear();

            while (!done)
            {
                ImTUI.Begin();

                EntityEditor.Edit(uf, this.issueTracker);

                if (ImTUI.Button("OK"))
                {
                    done = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    ok = false;
                    done = true;
                }

                ImTUI.End(!done);
            }

            if (ok)
            {
                ImTUI.Begin();
                try
                {
                    var user = userMgmt.RegisterNewUser(uf.ID, uf.Username, uf.FirstName, uf.LastName, uf.EmailAddr, uf.Telephone);
                    if (user != null)
                    {
                        ImTUI.MessageBox("Added new user {0} (#{1})!", user.Username, user.ID);
                    }
                    else
                    {
                        ImTUI.MessageBox("Failed to register user!");
                    }
                }
                catch (UserManagementException ex)
                {
                    ImTUI.MessageBox("Failed to register user!\n{0}", ex.Message);
                }

                ImTUI.End(true);
            }
        }

        /// <summary>
        /// User deletion confirmation screen.
        /// </summary>
        /// <param name="session">Session.</param>
        /// <param name="userMgmt">User management interface.</param>
        /// <param name="userID">User ID.</param>
        private void DeleteUser(IssueTrackerSession session, IUserManagement userMgmt, int userID)
        {
            string ask_msg = @"Are you sure you want to delete this user?
ID: {0} Username: {1}
First name: {2} Last name: {3}
Email address: {4}
Telephone: {5}";
            bool done = false;
            var user = userMgmt.GetUserByID(userID);
            ImTUI.ForceClear();
            if (user != null)
            {
                bool res = false;
                AggregateException error = null;
                while (!done)
                {
                    ImTUI.MessageBoxButtons selection;
                    ImTUI.Begin();
                    if (ImTUI.MessageBox(
                        ImTUI.MessageBoxType.YesNo,
                        out selection,
                        ask_msg,
                        user.ID,
                        user.Username,
                        user.FirstName,
                        user.LastName,
                        user.EmailAddr,
                        user.Telephone))
                    {
                        switch (selection)
                        {
                            case ImTUI.MessageBoxButtons.Yes:
                                try
                                {
                                    userMgmt.DeleteUser(userID);
                                    res = true;
                                }
                                catch (AggregateException ex)
                                {
                                    res = false;
                                    error = ex;
                                }

                                break;
                            case ImTUI.MessageBoxButtons.No:
                                break;
                        }

                        done = true;
                    }

                    ImTUI.End(true);
                }

                ImTUI.Begin();
                ImTUI.MessageBox(res ? "User deletion has succeded!" : "User deletion has failed!");
                if (!res && error != null)
                {
                    ImTUI.Text("The following errors occured:");
                    foreach (var ex in error.InnerExceptions)
                    {
                        ImTUI.Text($"- {ex.Message}");
                    }
                }

                ImTUI.End(true);
            }
            else
            {
                ImTUI.Begin();
                ImTUI.MessageBox("The delete failed, because user ID #{0} doesn't exist!", userID);
                ImTUI.End(true);
            }

            ImTUI.ForceClear();
        }

        /// <summary>
        /// Main user management screen.
        /// </summary>
        /// <param name="session">Session.</param>
        private void UserManagement(IssueTrackerSession session)
        {
            bool done = false;
            var userMgmt = this.issueTracker.GetUserManagement();
            //// users = userMgmt.GetAllUsers();
            ImTUI.ForceClear();
            var tableWidget = new UserSelectionWidget(this.issueTracker);

            while (!done)
            {
                ImTUI.Begin();

                if (!tableWidget.Update())
                {
                    ImTUI.Text("The list is empty!");
                }

                // NOTE(danielm): ImTUI has a bug, that if you press a key and that key will never be handled in a frame
                // then ImTUI.End will never block the execution. This means that in this case the loop becomes an infinite
                // loop, burning cycles at max speed.
                // TODO(danielm): this could be fixed by flushing the keypress queue if they're not handled within a
                // few frames.
                switch (ImTUI.MenuBar("Help", "Create", "Delete", "Modify", "Refresh", "---", "---", "---", "---", "Back"))
                {
                    case 2:
                        ImTUI.Unfocus();
                        this.UserManagementCreate(session, userMgmt);
                        break;
                    case 3:
                        if (tableWidget.SelectedEntity != null)
                        {
                            this.DeleteUser(session, userMgmt, tableWidget.GetSelectedEntity<UserInfo>().ID);
                        }

                        break;
                    case 4:
                        if (tableWidget.SelectedEntity != null)
                        {
                            this.ModifyUser(tableWidget.GetSelectedEntity<UserInfo>().ID);
                        }

                        break;
                    case 5:
                        tableWidget.InvalidateCache();
                        break;
                    case 10:
                        done = true;
                        this.SwitchScreen(Screen.MainMenu);
                        break;
                    default:
                        break;
                }

                ImTUI.End(!done);
            }
        }
    }
}

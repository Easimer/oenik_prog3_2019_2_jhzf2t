﻿// <copyright file="ClientProjectManagement.cs" company="easimer.net">
// Copyright (c) easimer.net. All rights reserved.
// </copyright>

namespace Net.Easimer.Prog3.Client
{
    using System;
    using System.Collections.Generic;
    using Net.Easimer.Prog3.IssueTracker;

    /// <summary>
    /// Implementation of project management screens.
    /// </summary>
    internal partial class Client
    {
        // TODO(danielm): move this elsewhere
        private void ModifyEntityCommon(int entityID, string[] options, Action<int>[] actions, string entityName = "object")
        {
            ImTUI.ForceClear();
            bool done = false;
            string[] optionsFilled = new string[10];
            Action<int>[] actionsFilled = new Action<int>[10];
            int iOption;

            if (options.Length != actions.Length)
            {
                throw new ArgumentException("Arrays sizes don't match");
            }

            // NOTE(danielm): Partial impl so we capture iOption's value below
            Func<int, Action<int>> λcbwopt = optidx => x =>
            {
                actions[optidx](x);
                done = true;
            };

            for (iOption = 0; iOption < options.Length; iOption++)
            {
                optionsFilled[iOption] = options[iOption];
                actionsFilled[iOption] = λcbwopt(iOption);
            }

            for (; iOption < 9; iOption++)
            {
                optionsFilled[iOption] = "---";
                actionsFilled[iOption] = x => { };
            }

            optionsFilled[9] = "Back";
            actionsFilled[9] = x => { done = true; };

            while (!done)
            {
                ImTUI.Begin();
                ImTUI.Text($"Select from below the way you want to modify this {entityName}");
                int option = ImTUI.MenuBar(optionsFilled);
                if (option > 0 && option <= 10)
                {
                    actionsFilled[--option](entityID);
                }

                ImTUI.End(!done);
            }
        }

        private void SetHomepage(int projectID)
        {
            bool done = false;
            bool ok = true;
            var projMgmt = this.issueTracker.GetProjectManagement();
            var proj = projMgmt.GetByID(projectID);
            string hp = proj.Homepage;
            bool present = hp != null;

            ImTUI.ForceClear();

            while (!done)
            {
                ImTUI.Begin();
                ImTUI.Text($"Setting homepage of project #{projectID} '${proj.Name}'");

                if (ImTUI.Checkbox("Has homepage", ref present))
                {
                    if (hp == null)
                    {
                        hp = string.Empty;
                    }

                    ImTUI.Input("Name", ref hp);
                }

                if (ImTUI.Button("OK"))
                {
                    done = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    done = true;
                    ok = false;
                }

                ImTUI.End(!done);
            }

            if (ok)
            {
                if (!present)
                {
                    hp = null;
                }

                projMgmt.SetHomepage(proj, hp);
            }

            ImTUI.ForceClear();
        }

        private void RenameProject(int projectID)
        {
            bool done = false;
            bool ok = true;
            var projMgmt = this.issueTracker.GetProjectManagement();
            var proj = projMgmt.GetByID(projectID);
            string name = proj.Name;

            ImTUI.ForceClear();

            while (!done)
            {
                ImTUI.Begin();
                ImTUI.Text($"Renaming project #{projectID} '${proj.Name}'");
                ImTUI.Input("Name", ref name);
                if (ImTUI.Button("OK"))
                {
                    done = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    done = true;
                    ok = false;
                }

                ImTUI.End(!done);
            }

            if (ok)
            {
                projMgmt.Rename(proj, name);
            }

            ImTUI.ForceClear();
        }

        private void ModifyProject(int projectID)
        {
            var opt = new string[]
            {
                "Rename",
                "SetHomePg",
            };

            var act = new Action<int>[]
            {
                    p => this.RenameProject(p),
                    p => this.SetHomepage(p),
            };

            this.ModifyEntityCommon(projectID, opt, act, "project");
        }

        private bool ProjectManagementCreate(IssueTrackerSession session, IProjectManagement projMgmt)
        {
            bool ret = false;
            bool done = false;
            bool ok = true;

            var ent = new ProjectInfo();

            ImTUI.ForceClear();
            while (!done)
            {
                ImTUI.Begin();
                EntityEditor.Edit<ProjectInfo>(ent, this.issueTracker);

                if (ImTUI.Button("OK"))
                {
                    done = true;
                }
                else if (ImTUI.Button("Cancel"))
                {
                    ok = false;
                    done = true;
                }

                ImTUI.End(!done);
            }

            if (ok)
            {
                ImTUI.Begin();
                try
                {
                    int? ownerID = ent.OwnerRef != null ? (int?)ent.OwnerRef.ID : null;
                    int? parentID = ent.ParentRef != null ? (int?)ent.ParentRef.ID : null;
                    var proj = projMgmt.CreateProject(ent.ID, ent.Name, ent.Homepage, ownerID, parentID, ent.CreationDate);
                    if (proj != null)
                    {
                        ImTUI.MessageBox("Created new project {0} (#{1})!", proj.Name, proj.ID);
                        ret = true;
                    }
                    else
                    {
                        ImTUI.MessageBox("Failed to create project!\n");
                    }
                }
                catch (ProjectManagementException ex)
                {
                    ImTUI.MessageBox("Failed to create project!\n{0}", ex.Message);
                }

                ImTUI.End();
                ImTUI.BlockUntilUserInput();
            }

            ImTUI.ResetScreen();

            return ret;
        }

        /// <summary>
        /// Project deletion confirmation screen.
        /// </summary>
        /// <param name="session">Session.</param>
        /// <param name="projMgmt">User management interface.</param>
        /// <param name="projID">Project ID.</param>
        private bool DeleteProject(IssueTrackerSession session, IProjectManagement projMgmt, int projID)
        {
            bool ret = false;
            string ask_msg = @"Are you sure you want to delete this project?
ID: {0} Name: {1}
Homepage: {2}
Owned by user: {3}
Part of the project: {4}";
            bool done = false;
            var proj = projMgmt.GetByID(projID);
            ImTUI.ForceClear();
            if (proj != null)
            {
                bool res = false;
                while (!done)
                {
                    ImTUI.MessageBoxButtons selection;
                    ImTUI.Begin();
                    if (ImTUI.MessageBox(
                        ImTUI.MessageBoxType.YesNo,
                        out selection,
                        ask_msg,
                        proj.ID,
                        proj.Name,
                        proj.Homepage,
                        proj.OwnerRef != null ? proj.OwnerRef.Username : "(none)",
                        proj.ParentRef != null ? proj.ParentRef.Name : "(none)"))
                    {
                        switch (selection)
                        {
                            case ImTUI.MessageBoxButtons.Yes:
                                projMgmt.DeleteByID(projID);
                                res = true;
                                break;
                            case ImTUI.MessageBoxButtons.No:
                                break;
                        }

                        done = true;
                    }

                    ImTUI.End(true);
                }

                ImTUI.Begin();
                ImTUI.MessageBox(res ? "Project deletion has succeded!" : "Project deletion has failed!");
                ImTUI.End(true);
                ret = res;
            }
            else
            {
                ImTUI.Begin();
                ImTUI.MessageBox("The delete failed, because project ID #{0} doesn't exist!", projID);
                ImTUI.End(true);
            }

            ImTUI.ResetScreen();

            return ret;
        }

        private bool SetOwner(IssueTrackerSession session, IProjectManagement projMgmt, int projectID)
        {
            bool ret = false;
            bool done = false;
            var userMgmt = this.issueTracker.GetUserManagement();
            var usersTable = new UserSelectionWidget(this.issueTracker);
            int selectedID = -1;
            UserInfo selectedUser = null;
            string selectedUsername = string.Empty;

            if (projectID != -1)
            {
                var proj = projMgmt.GetByID(projectID);

                while (!done)
                {
                    ImTUI.Begin();

                    usersTable.Update();

                    if (usersTable.SelectedEntity != null)
                    {
                        selectedUser = usersTable.GetSelectedEntity<UserInfo>();
                        selectedID = selectedUser.ID;
                        selectedUsername = selectedUser.Username;
                    }

                    ImTUI.Text("\nSelected: #{0} {1}", selectedID, selectedUsername);

                    if (ImTUI.Button("OK"))
                    {
                        if (selectedUser != null)
                        {
                            done = true;
                            projMgmt.SetOwner(proj, selectedUser);
                            ret = true;
                        }
                    }
                    else if (ImTUI.Button("Cancel"))
                    {
                        done = true;
                    }

                    ImTUI.End(!done);
                }
            }
            else
            {
                ImTUI.MessageBox("Project ID was invalid!");
                ImTUI.BlockUntilUserInput();
            }

            ImTUI.ResetScreen();

            return ret;
        }

        private void ProjectManagement(IssueTrackerSession session)
        {
            bool done = false;
            bool requestRefresh = false;
            var projMgmt = this.issueTracker.GetProjectManagement();
            IEnumerable<ProjectInfo> projects;

            projects = projMgmt.GetAllProjects();

            var tableWidget = new ProjectSelectionWidget(this.issueTracker);

            while (!done)
            {
                ImTUI.Begin();

                // Refresh list of projects if it was requested
                if (requestRefresh)
                {
                    ImTUI.Text("Refreshing...");
                    projects = projMgmt.GetAllProjects();
                    requestRefresh = false;
                    tableWidget.InvalidateCache();
                    ImTUI.End();
                }
                else
                {
                    if (!tableWidget.Update())
                    {
                        ImTUI.Button("The list is empty!");
                    }

                    switch (ImTUI.MenuBar("Help", "Create", "Delete", "Modify", "Refresh", "Issues", "More", "SetOwn", "---", "Back"))
                    {
                        case 2:
                            if (this.ProjectManagementCreate(session, projMgmt))
                            {
                                requestRefresh = true;
                            }

                            break;
                        case 3:
                            if (tableWidget.SelectedEntity != null)
                            {
                                requestRefresh = this.DeleteProject(session, projMgmt, tableWidget.GetSelectedEntity<ProjectInfo>().ID);
                                requestRefresh = true;
                            }

                            break;
                        case 4:
                            if (tableWidget.SelectedEntity != null)
                            {
                                this.ModifyProject(tableWidget.GetSelectedEntity<ProjectInfo>().ID);
                                requestRefresh = true;
                            }

                            break;
                        case 5:
                            requestRefresh = true;
                            break;
                        case 6:
                            if (tableWidget.SelectedEntity != null)
                            {
                                this.IssueManagement(session, tableWidget.GetSelectedEntity<ProjectInfo>().ID);
                                requestRefresh = true;
                            }

                            break;
                        case 8:
                            requestRefresh = true;
                            if (tableWidget.SelectedEntity != null)
                            {
                                this.SetOwner(session, projMgmt, tableWidget.GetSelectedEntity<ProjectInfo>().ID);
                                requestRefresh = true;
                            }

                            break;
                        case 10:
                            done = true;
                            this.SwitchScreen(Screen.MainMenu);
                            break;
                        default:
                            break;
                    }

                    ImTUI.End(!done);
                }
            }
        }
    }
}

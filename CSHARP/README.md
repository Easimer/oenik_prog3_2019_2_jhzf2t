# IssueTracker

User manual for IssueTracker as of 2019/10/05.

## Using the console application

At startup a login screen will be shown. You can move between the fields using
the TAB and Shift-TAB keys. Pressing TAB jumps to the next, while Shift-TAB
jumps to the previous UI element. The selected menu element is enclosed in
square brackets. If a text input field is selected then the user can type into
it using the keyboard. If the selected element is a button then it can be
pushed using the Return key.

The program is in demo mode by default. You can login using any
username/password combination, provided that the password is at least 5
characters long, the second character is a 'u' and the fifth character is an
'a'. This makes 'durga' a valid password for example. These credentials are
mapped to user #1, thus every operation performed will be made in their name.

### Main menu
The main menu provides the following options:

* *List of projects*: lists all projects; project management.
* *List of users*: lists all users; user management.
* *List of all issues*: lists all issues.
* *Statistics: issues per project*: generates a statistical report about all projects.
* *Statistics: issues per user*: generates a statistical report about all users.
* *Exit*: exits to the login screen.

### General usage of the management screens
The management screens consist of two parts: a table in which all items are
shown and a menu bar at the bottom. You can move between the rows using the
familiar TAB/Shift-TAB keys, while the key Return selects the row at which the
cursor points. After selecting a row an asterisk ('*') will appear before it.
The row pointed to by the cursor is marked by a '>' character.

At the bottom of the screen a menu bar can be found showing all the operations
the user can perform. Pushing the key 'F1' performs the first operation,
pushing 'F2' performs the second and so on. Options labeled as '---' do
nothing. Every management screen can be exited using the 'F10' key.

**BUG**: if an ID field appears in any screen, then that's a mistake and cannot
be edited, or if it can be edited it's value won't be saved!

#### Managing projects
Operations that can be performed on projects:

*  *Help*: shows help information about managing projects (NYI);
*  *Create*: create a project;
*  *Delete*: delete the selected project;
*  *Modify*: modify the selected project;
*  *Refresh*: refresh the list of projects;
*  *Issues*: manage the issues of the selected project,
*  *More*: shows further information about the selected project (NYI) and
*  *SetOwn*: set the owner of the selected project.

#### Managing users
Operations that can be performed on users:

*  *Help*: shows help information about managing users (NYI);
*  *Create*: register a new user;
*  *Delete*: delete the selected user,
*  *Modify*: modify the informations of the selected user and
*  *Refresh*: refresh the list of users.

#### Managing issues
Operations that can be performed on issues in *global mode*:

*  *Delete*: delete the selected issue;
*  *Modify*: modify the selected issue;
*  *Refresh*: refresh the list of issues;
*  *Close*: close the selected issue,
*  *More*: shows further information about the selected issue (NYI) and
*  *Assign*: assign a user to the selected issue.

Further operations that can be performed on issues in *project mode*:

*  *Create*: create an issue.

**ATTENTION**: managing issues related to a project (a.k.a. entering "project
mode") can be initiated from the project manager by using the 'Issues' menu
option.